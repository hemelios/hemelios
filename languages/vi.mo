��    #      4      L      L  +   M  *   y  *   �  �   �     |     �     �     �     �  
   �     �     �     �     �               -     <  	   C     M     ^     d     q  	   �     �  W   �  '   �  m         �  	   �     �  @   �     �  )   �  �    /   �	  /   �	  /   
  �   O
     B     Y     k     �     �     �     �     �     �  0   �       (   +     T     g  
   n     y     �     �     �     �     �  �   �  .   �  �   �     h  
   l     w  P   {  	   �  6   �   <i class="icon icon-chat-1"></i> % Comments <i class="icon icon-chat-1"></i> 0 Comment <i class="icon icon-chat-1"></i> 1 Comment <span>Update Required</span> To play the media you will need to either update your browser to a recent version or update your <a href="%s" target="_blank">Flash plugin</a>. BACK TO HOMEPAGE Cancel reply Could not save file Descriptions Edit Loading... Message Name New Product No image URL has been entered. POST YOUR COMMENT POST YOUR COMMENT TO %s Page Not Found Pages: Read more Related Products Reply SEND MESSAGE Search results for " Search... See More Things Sorry, but nothing matched your search terms. Please try again with different keywords. The page you’re looking for not found This variable allows you to set the maximum amount of items displayed at a time with the widest browser width View View More Views We are currently in maintenance mode, please check back shortly. Website Your email address will not be published. Project-Id-Version: Tax meta class
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-04-25 09:31+0100
PO-Revision-Date: Fri Mar 18 2016 17:53:45 GMT+0700 (SE Asia Standard Time)
Last-Translator: admin <pnlongpro@gmail.com>
Language-Team: David THOMAS <anou@smol.org>
Language: Vietnamese
Plural-Forms: nplurals=1; plural=0
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Generator: Loco - https://localise.biz/
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ../../Tax-meta-class
X-Loco-Target-Locale: vi_VN <i class="icon icon-chat-1"></i> % Bình luận <i class="icon icon-chat-1"></i> 0 bình luận <i class="icon icon-chat-1"></i> 1 Bình luận <span>Cần cập nhật</span>Để chạy các đa phương tiện, bạn sẽ cần phải hoặc cập nhật trình duyệt của bạn lên phiên bản mới hoặc cập nhật của bạn<a href="%s" target="_blank">Flash plugin</a>. QUAY LẠI TRANG CHỦ Hủy trả lời Không thể lưu tập tin Thông tin chi tiết Sửa Đang tải.... Thông điệp Tên Sản phẩm mời Không có đường dẫn ảnh được nhập Đăng bình luận của bạn Đăng bình luận của bạn vào  %s Không tìm thấy Trang: Xem tiếp Sản phẩm liên quan Trả lời Gửi thông điệp Kết quả tìm kiếm của Tìm kiếm... Xem thêm Những điều Xin lỗi, nhưng không có gì phù hợp với điều kiện tìm kiếm của bạn. Vui lòng thử lại với từ khoá khác nhau. Trang bạn đang tìm đã không tìm thấy Biến này cho phép bạn thiết lập số lượng tối đa của các mục hiển thị tại một thời điểm với chiều rộng của trình duyệt rộng nhất Xem Xem tiếp Xem Chúng tôi hiện đang trong chế độ bảo trì, vui lòng quay lại sau. Trang web Địa chỉ email của bạn sẽ không công khai. 