(function ($) {
	"use strict";
	var HemeliosPanelStyleSelector = {
		htmlTag: {
			wrapper: '#panel-style-selector'
		},
		vars: {
			isLoading: false
		},
		initialize: function () {
			HemeliosPanelStyleSelector.build();
		},
		build: function () {
			$.ajax({
				type: 'POST',
				data: 'action=panel_selector',
				url: hemelios_framework_ajax_url,
				success: function (html) {
					$('body').append(html);
					HemeliosPanelStyleSelector.events();
				},
				error: function (html) {

				}
			});
		},
		events: function () {
			$('.panel-selector-open', HemeliosPanelStyleSelector.htmlTag.wrapper).click(function () {
				$('.panel-wrapper', HemeliosPanelStyleSelector.htmlTag.wrapper).toggleClass('in');
			});
			$('.panel-selector-open', HemeliosPanelStyleSelector.htmlTag.wrapper).hover(function () {
				$('i', this).addClass('fa-spin');
			}, function () {
				$('i', this).removeClass('fa-spin');
			});

			HemeliosPanelStyleSelector.layout();
			HemeliosPanelStyleSelector.background();
			HemeliosPanelStyleSelector.reset();
            HemeliosPanelStyleSelector.rtl();
			HemeliosPanelStyleSelector.primary_color();

		},
		layout: function () {

			if ($('body').hasClass('boxed')) {
				$('a[data-value="boxed"]', HemeliosPanelStyleSelector.htmlTag.wrapper).addClass('active');
			} else {
				$('a[data-value="wide"]', HemeliosPanelStyleSelector.htmlTag.wrapper).addClass('active');
			}

			$('a[data-type="layout"]', HemeliosPanelStyleSelector.htmlTag.wrapper).click(function (event) {
				event.preventDefault();

				$('a[data-type="layout"]', HemeliosPanelStyleSelector.htmlTag.wrapper).removeClass('active');
				$(this).addClass('active');

				var layout = $(this).attr('data-value');
				if (layout == 'boxed') {
					$('body').addClass('boxed');
					$('.background-section').slideDown();
				} else {
					$('body').removeClass('boxed');
					$('.background-section').slideUp();
				}
                $('#wrapper-content').trigger(jQuery.Event('resize'));
			})

		},
		background: function () {
			$('.background-section').slideUp();

			$('.panel-primary-background li', HemeliosPanelStyleSelector.htmlTag.wrapper).click(function (event) {
				event.preventDefault();
				var name = $(this).data('name');
				var type = $(this).data('type');

				$('body').css({
					'background-image': 'url(' + hemelios_framework_theme_url + 'assets/images/theme-options/' + name + ')',
					'background-repeat': 'repeat',
					'background-position': 'center center',
					'background-attachment': 'scroll',
					'background-size': 'auto'
				});

                $('ul.panel-primary-background li', HemeliosPanelStyleSelector.htmlTag.wrapper).removeClass('active');
                $(this).addClass('active');

			})
		},
		show_loading           : function () {
			$('body').addClass('overflow-hidden');
			if ($('.loading-wrapper').length == 0) {
				$('body').append('<div class="loading-wrapper"><span class="spinner-double-section-far"></span></div>');
			}
		},
		hide_loading           : function () {
			$('.loading-wrapper').fadeOut(function () {
				$('.loading-wrapper').remove();
				$('body').removeClass('overflow-hidden');
			});
		},
		primary_color: function () {
			$('ul.panel-primary-color li', HemeliosPanelStyleSelector.htmlTag.wrapper).click(function (event) {
				event.preventDefault();
				var $this = $(this);
				if (HemeliosPanelStyleSelector.vars.isLoading) return;
				HemeliosPanelStyleSelector.vars.isLoading = true;
				var color = $(this).data('color');
				$('.panel-selector-open i', HemeliosPanelStyleSelector.htmlTag.wrapper).addClass('fa-spin');
				HemeliosPanelStyleSelector.show_loading();
				$.ajax({
					url: hemelios_framework_ajax_url,
					data: {
						action: 'custom_css_selector',
						primary_color: color
					},
					success: function (response) {
						HemeliosPanelStyleSelector.hide_loading();
						HemeliosPanelStyleSelector.vars.isLoading = false;

						$('ul.panel-primary-color li', HemeliosPanelStyleSelector.htmlTag.wrapper).removeClass('active');
						$this.addClass('active');

						if ($('style#color_ss').length == 0) {
							$('head').append('<style type="text/css" id="color_ss"></style>');
						}

						$('style#color_ss').html(response);
						$('.panel-selector-open i', HemeliosPanelStyleSelector.htmlTag.wrapper).removeClass('fa-spin');
					},
					error: function () {
						HemeliosPanelStyleSelector.hide_loading();
						HemeliosPanelStyleSelector.vars.isLoading = false;
						$('.panel-selector-open i', HemeliosPanelStyleSelector.htmlTag.wrapper).removeClass('fa-spin');
					}
				});

			});
		},
		reset: function () {
			$('#panel-selector-reset', HemeliosPanelStyleSelector.htmlTag.wrapper).click(function (event) {
				event.preventDefault();
				document.location.reload(true);
			})
		},
        rtl : function() {
            $('#panel-selector-rtl', HemeliosPanelStyleSelector.htmlTag.wrapper).click(function (event) {
                event.preventDefault();
                if (HemeliosPanelStyleSelector.vars.isLoading) return;
                HemeliosPanelStyleSelector.vars.isLoading = true;
                $('.panel-selector-open i', HemeliosPanelStyleSelector.htmlTag.wrapper).addClass('fa-spin');

                var mode = $(this).data('mode');
                if (mode == 'on') {
                    $('#rtl-css').remove();
                    $(this).data('mode','off');
                    $(this).text('RTL On')
                } else {
                    $('body').append("<link rel='stylesheet' id='rtl-css'  href='"+hemelios_framework_theme_url+"assets/css/rtl.min.css' type='text/css' media='all' />");
                    $(this).data('mode','on');
                    $(this).text('RTL Off')
                }

                HemeliosPanelStyleSelector.vars.isLoading = false;
                $('.panel-selector-open i', HemeliosPanelStyleSelector.htmlTag.wrapper).removeClass('fa-spin');
            })
        }
	};

	$(document).ready(function () {
		HemeliosPanelStyleSelector.initialize();
	});
})(jQuery);