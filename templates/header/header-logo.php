<?php
$hemelios_options = hemelios_option();

$prefix        = 'hemelios_';
$header_layout = hemelios_get_post_meta_box_option( $prefix . 'header_layout' );
if ( ( $header_layout === '' ) || ( $header_layout == '-1' ) ) {
	$header_layout = $hemelios_options['header_layout'];
}

$logo_meta = hemelios_get_post_meta_box_option( $prefix . 'custom_logo', 'type=image_advanced' );
$logo_url  = '';

if ($logo_meta != '' && $logo_meta !== array()) {
	foreach ( $logo_meta as $item ) {
		if ( isset( $item['full_url'] ) & !empty( $item['full_url'] ) ) {
			$logo_url = $item['full_url'];
			break;
		}
	}
}

if ( $logo_url === '' ) {
	$logo_url =  get_template_directory_uri()  . '/assets/images/theme-options/logo.png';

	if ( isset( $hemelios_options['dark_logo']['url'] ) && !empty( $hemelios_options['dark_logo']['url'] ) ) {
		$logo_url = $hemelios_options['dark_logo']['url'];
	}
}

if ( is_404() ) {
	$logo_url =  get_template_directory_uri()  . '/assets/images/theme-options/logo-light.png';
	$header_layout = 'header-1';
}

?>
<div class="header-logo">
	<a href="<?php echo esc_url( trailingslashit( home_url() ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> - <?php bloginfo( 'description' ); ?>" rel="home">
		<img src="<?php echo esc_url( $logo_url ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> - <?php bloginfo( 'description' ); ?>" />
	</a>
</div>