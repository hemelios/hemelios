<?php
$hemelios_options = hemelios_option();

$prefix                  = 'hemelios_';
$enable_header_customize = hemelios_get_post_meta_box_option( $prefix . 'enable_header_customize' );
$header_customize_text   = '';

if ( $enable_header_customize == '1' ) {
	$header_customize_text = hemelios_get_post_meta_box_option( $prefix . 'header_customize_text' );
} else {
	$header_customize_text = $hemelios_options['header_customize_text'];
}
?>
<?php if ( !empty( $header_customize_text ) ): ?>
	<div class="custom-text-wrapper header-customize-item">
		<?php echo apply_filters( 'header_customize_custom_text', $header_customize_text ); ?>
	</div>
<?php endif; ?>