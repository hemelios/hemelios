<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'title'  => esc_html__( 'Logo', 'hemelios' ),
		'desc'   => '',
		'icon'   => 'el el-leaf',
		'fields' => array(
			array(
				'id'       => 'logo',
				'type'     => 'media',
				'url'      => true,
				'title'    => esc_html__( 'Logo', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload your logo here.', 'hemelios' ),
				'desc'     => '',
				'default'  => array(
					'url' => get_template_directory_uri() . '/assets/images/theme-options/logo.png'
				)
			),
			array(
				'id'       => 'light_logo',
				'type'     => 'media',
				'url'      => false,
				'title'    => esc_html__( 'Light Logo', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload a light version of your logo here.', 'hemelios' ),
				'desc'     => '',
				'default'  => array(
					'url' => get_template_directory_uri() . '/assets/images/theme-options/logo-light.png'
				)
			),
			array(
				'id'       => 'dark_logo',
				'type'     => 'media',
				'url'      => false,
				'title'    => esc_html__( 'Dark Logo', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload a dark version of your logo here.', 'hemelios' ),
				'desc'     => '',
				'default'  => array(
					'url' => get_template_directory_uri() . '/assets/images/theme-options/logo-dark.png'
				)
			),
			array(
				'id'       => 'logo_max_height',
				'type'     => 'dimensions',
				'title'    => esc_html__( 'Logo Max Height', 'hemelios' ),
				'subtitle' => esc_html__( 'You can set a max height for the logo here.', 'hemelios' ),
				'units'    => 'px',
				'width'    => false,
				'default'  => array(
					'Height' => '80'
				)
			),

			array(
				'id'       => 'logo_padding',
				'type'     => 'text',
				'title'    => esc_html__( 'Logo Top/Bottom Padding', 'hemelios' ),
				'subtitle' => esc_html__( 'This must be numeric (no px). leave balnk for default.', 'hemelios' ),
				'desc'     => esc_html__( 'If you would like to override the default logo top/bottom padding, then you can do so here.', 'hemelios' ),
				'validate' => 'numeric',
				'default'  => '0',
			),
		)
	) );