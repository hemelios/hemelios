<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'title'  => esc_html__( 'Shop', 'hemelios' ),
		'desc'   => '',
		'icon'   => 'el el-shopping-cart',
		'fields' => array(
			array(
				'id'       => 'add_to_cart_animation',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Add-To-Cart Animation', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable add-to-cart animation.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),
			array(
				'id'       => 'product_per_page',
				'type'     => 'text',
				'title'    => esc_html__( 'Products Per Page', 'hemelios' ),
				'subtitle' => esc_html__( 'This must be numeric or empty (default 12).', 'hemelios' ),
				'desc'     => esc_html__( 'Set products per page in archive product.', 'hemelios' ),
				'validate' => 'numeric',
				'default'  => '16',
			),
			array(
				'id'       => 'product_display_columns',
				'type'     => 'select',
				'title'    => esc_html__( 'Product Display Columns', 'hemelios' ),
				'subtitle' => esc_html__( 'Choose the number of columns to display on shop/category pages.', 'hemelios' ),
				'options'  => array(
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				),
				'desc'     => '',
				'default'  => '4'
			),
			array(
				'id'       => 'product_show_rating',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Show Rating', 'hemelios' ),
				'subtitle' => esc_html__( 'Show or hide rating product.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),


			array(
				'id'       => 'product_sale_flash_mode',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Sale Flash Mode', 'hemelios' ),
				'subtitle' => esc_html__( 'Choose sale flash mode.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'text' => 'Text', 'percent' => 'Percent' ),
				'default'  => 'text'
			),

			array(
				'id'       => 'product_show_result_count',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Show Result Count', 'hemelios' ),
				'subtitle' => esc_html__( 'Show or hide result count in archive product.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),
			array(
				'id'       => 'product_show_catalog_ordering',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Show Catalog Ordering', 'hemelios' ),
				'subtitle' => esc_html__( 'Show or hide catalog ordering.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),
			array(
				'id'       => 'product_quick_view',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Quick View', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable quick view.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'Enable', '0' => 'Disable' ),
				'default'  => '1'
			),
		)
	) );