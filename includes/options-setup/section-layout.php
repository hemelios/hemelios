<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'title'  => esc_html__( 'Layout Setting', 'hemelios' ),
		'desc'   => '',
		'icon'   => 'el el-th',
		'fields' => array(
			array(
				'id'     => 'page_heading',
				'title'  => esc_html__( 'Page Title', 'hemelios' ),
				'type'   => 'section',
				'indent' => true
			),

			array(
				'id'       => 'page_title_text_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Page Title Text Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Pick a text color for page title.', 'hemelios' ),
				'default'  => '#FFFFFF',
				'validate' => 'color',
			),
			array(
				'id'       => 'page_title_bg_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Page Title Background Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Pick a background color for page title.', 'hemelios' ),
				'default'  => '#2a2a2a',
				'validate' => 'color',
			),
			array(
				'id'       => 'page_title_overlay_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Page Title Background Overlay Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Pick a background overlay color for page title.', 'hemelios' ),
				'default'  => '#000',
				'validate' => 'color',
			),

			array(
				'id'       => 'page_title_overlay_opacity',
				'type'     => 'slider',
				'title'    => esc_html__( 'Page Title Background Overlay Opacity', 'hemelios' ),
				'subtitle' => esc_html__( 'Set the opacity level of the overlay.', 'hemelios' ),
				'default'  => '50',
				"min"      => 0,
				"step"     => 1,
				"max"      => 100
			),

		)
	) );