<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

$products_title_bg_url     = get_template_directory_uri() . '/assets/images/bg-portfolio-title.jpg';

global $osOpt;

$osOpt->addSection(
	array(
		'title'      => esc_html__( 'Products', 'hemelios' ),
		'desc'       => '',
		'icon'       => 'el el-asterisk',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'product_layout',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Layout', 'hemelios' ),
				'subtitle' => esc_html__( 'Select page layout.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'full' => 'Full Width', 'container' => 'Container', 'container-fluid' => 'Container Fluid' ),
				'default'  => 'container'
			),
			array(
				'id'       => 'product_sidebar',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Sidebar', 'hemelios' ),
				'subtitle' => esc_html__( 'Set sidebar style.', 'hemelios' ),
				'desc'     => '',
				'options'  => array(
					'none'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-none.png' ),
					'left'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-left.png' ),
					'right' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-right.png' ),
					'both'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-both.png' ),
				),
				'default'  => 'left'
			),

			array(
				'id'       => 'product_sidebar_width',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Sidebar Width', 'hemelios' ),
				'subtitle' => esc_html__( 'Set sidebar width.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'small' => 'Small (1/4)', 'large' => 'Large (1/3)' ),
				'default'  => 'small',
				'required' => array( 'product_sidebar', '=', array( 'left', 'both', 'right' ) ),
			),


			array(
				'id'       => 'product_left_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Left Sidebar', 'hemelios' ),
				'subtitle' => "Choose the default left sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'sidebar-products',
				'required' => array( 'product_sidebar', '=', array( 'left', 'both' ) ),
			),
			array(
				'id'       => 'product_right_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Right Sidebar', 'hemelios' ),
				'subtitle' => "Choose the default right sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'sidebar-2',
				'required' => array( 'product_sidebar', '=', array( 'right', 'both' ) ),
			),
			array(
				'id'       => 'show_product_title',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Show Products Title', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable products title.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),
			array(
				'id'       => 'product_title_height',
				'type'     => 'dimensions',
				'title'    => esc_html__( 'Products Title Height', 'hemelios' ),
				'subtitle' => esc_html__( 'This must be numeric (no px) or empty.', 'hemelios' ),
				'desc'     => esc_html__( 'You can set a height for the products title here.', 'hemelios' ),
				'units'    => 'px',
				'width'    => false,
				'default'  => array(
					'height' => '300'
				)
			),
			array(
				'id'       => 'breadcrumbs_in_product_title',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Products Breadcrumbs', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable breadcrumbs in products title.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),

			array(
				'id'       => 'product_breadcrumbs_position',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Products Breadcrumbs Positions', 'hemelios' ),
				'subtitle' => esc_html__( 'Select products breadcrumbs positions.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '0' => 'Left', '1' => 'Center', '2' => 'Right' ),
				'required' => array( 'breadcrumbs_in_product_title', '=', array( '1' ) ),
				'default'  => '0'
			),

			array(
				'id'       => 'product_title_bg_image',
				'type'     => 'media',
				'url'      => true,
				'title'    => esc_html__( 'Products Title Background', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload products title background.', 'hemelios' ),
				'desc'     => '',
				'default'  => array(
					'url' => $products_title_bg_url
				)
			),

		)
	) );