<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */


$archive_shop_title_bg_url = get_template_directory_uri() . '/assets/images/bg-shop-title.jpg';

global $osOpt;

$osOpt->addSection(
	array(
		'title'      => esc_html__( 'Single Product Options', 'hemelios' ),
		'desc'       => '',
		'icon'       => 'el el-shopping-cart',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'single_product_layout',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Single Product Layout', 'hemelios' ),
				'subtitle' => esc_html__( 'Select single product layout.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'full' => 'Full Width', 'container' => 'Container', 'container-fluid' => 'Container Fluid' ),
				'default'  => 'container'
			),
			array(
				'id'       => 'single_product_sidebar',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Single Product Sidebar', 'hemelios' ),
				'subtitle' => esc_html__( 'Set single product sidebar.', 'hemelios' ),
				'desc'     => '',
				'options'  => array(
					'none'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-none.png' ),
					'left'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-left.png' ),
					'right' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-right.png' ),
					'both'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-both.png' ),
				),
				'default'  => 'none'
			),
			array(
				'id'       => 'single_product_sidebar_width',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Single Product Sidebar Width', 'hemelios' ),
				'subtitle' => esc_html__( 'Set sidebar width.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'small' => 'Small (1/4)', 'large' => 'Large (1/3)' ),
				'default'  => 'small',
				'required' => array( 'single_product_sidebar', '=', array( 'left', 'both', 'right' ) ),
			),
			array(
				'id'       => 'single_product_left_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Single Product Left Sidebar', 'hemelios' ),
				'subtitle' => "Choose the default single product left sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'sidebar-1',
				'required' => array( 'single_product_sidebar', '=', array( 'left', 'both' ) ),
			),
			array(
				'id'       => 'single_product_right_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Single Product Right Sidebar', 'hemelios' ),
				'subtitle' => "Choose the default single product right sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'sidebar-2',
				'required' => array( 'single_product_sidebar', '=', array( 'right', 'both' ) ),
			),
			array(
				'id'       => 'show_single_product_title',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Show Single Title', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable single product title.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),
			array(
				'id'       => 'single_product_title_height',
				'type'     => 'dimensions',
				'title'    => esc_html__( 'Single Product Title Height', 'hemelios' ),
				'subtitle' => esc_html__( 'This must be numeric (no px) or empty.', 'hemelios' ),
				'desc'     => esc_html__( 'You can set a height for the single product title here.', 'hemelios' ),
				'required' => array( 'show_single_product_title', '=', array( '1' ) ),
				'units'    => 'px',
				'width'    => false,
				'default'  => array(
					'height' => '420'
				)
			),

			array(
				'id'       => 'breadcrumbs_in_single_product_title',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Single Product Breadcrumbs', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable breadcrumbs in single product title.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'required' => array( 'show_single_product_title', '=', array( '1' ) ),
				'default'  => '1'
			),

			array(
				'id'       => 'single_product_breadcrumbs_position',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Single Product Breadcrumbs Positions', 'hemelios' ),
				'subtitle' => esc_html__( 'Select single product breadcrumbs positions.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '0' => 'Left', '1' => 'Center', '2' => 'Right' ),
				'required' => array( 'breadcrumbs_in_single_product_title', '=', array( '1' ) ),
				'default'  => '1'
			),

			array(
				'id'       => 'single_product_title_bg_image',
				'type'     => 'media',
				'url'      => false,
				'title'    => esc_html__( 'Single Product Title Background', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload single product title background.', 'hemelios' ),
				'desc'     => '',
				'default'  => array(
					'url' => $archive_shop_title_bg_url
				)
			),

			array(
				'id'   => 'sing_product_divide',
				'type' => 'divide'
			),


			array(
				'id'       => 'related_product_count',
				'type'     => 'text',
				'title'    => esc_html__( 'Related Product Total Record', 'hemelios' ),
				'subtitle' => esc_html__( 'Total record of related product.', 'hemelios' ),
				'validate' => 'number',
				'default'  => '6',
			),

			array(
				'id'       => 'related_product_display_columns',
				'type'     => 'select',
				'title'    => esc_html__( 'Related Product Display Columns', 'hemelios' ),
				'subtitle' => esc_html__( 'Choose the number of columns to display on related product.', 'hemelios' ),
				'options'  => array(
					'3' => '3',
					'4' => '4',
				),
				'desc'     => '',
				'default'  => '4'
			),

			array(
				'id'      => 'related_product_condition',
				'type'    => 'checkbox',
				'title'   => esc_html__( 'Related Product Condition', 'hemelios' ),
				'options' => array(
					'category' => esc_html__( 'Same Category', 'hemelios' ),
					'tag'      => esc_html__( 'Same Tag', 'hemelios' ),
				),
				'default' => array(
					'category' => '1',
					'tag'      => '1',
				),
			),
		)
	) );