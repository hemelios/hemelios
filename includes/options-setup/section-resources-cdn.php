<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'title'  => esc_html__( 'Resources CDN', 'hemelios' ),
		'desc'   => '',
		'icon'   => 'el el-random',
		'fields' => array(
			array(
				'id'       => 'cdn_bootstrap_js',
				'type'     => 'text',
				'title'    => esc_html__( 'CDN Bootstrap Script', 'hemelios' ),
				'subtitle' => esc_html__( 'Url CDN Bootstrap script.', 'hemelios' ),
				'desc'     => '',
				'default'  => '',
			),

			array(
				'id'       => 'cdn_bootstrap_css',
				'type'     => 'text',
				'title'    => esc_html__( 'CDN Bootstrap Stylesheet', 'hemelios' ),
				'subtitle' => esc_html__( 'Url CDN Bootstrap stylesheet.', 'hemelios' ),
				'desc'     => '',
				'default'  => '',
			),

			array(
				'id'       => 'cdn_font_awesome',
				'type'     => 'text',
				'title'    => esc_html__( 'CDN Font Awesome', 'hemelios' ),
				'subtitle' => esc_html__( 'Url CDN Font awesome.', 'hemelios' ),
				'desc'     => '',
				'default'  => '',
			),

		)
	) );