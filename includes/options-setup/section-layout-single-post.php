<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */


$archive_title_bg_url      = get_template_directory_uri() . '/assets/images/bg-archive-title.jpg';

global $osOpt;

$osOpt->addSection(
	array(
		'title'      => esc_html__( 'Single Post Options', 'hemelios' ),
		'desc'       => '',
		'subsection' => true,
		'icon'       => 'el el-th-large',
		'fields'     => array(
			array(
				'id'       => 'single_blog_layout',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Layout', 'hemelios' ),
				'subtitle' => esc_html__( 'Select single blog layout.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'full' => 'Full Width', 'container' => 'Container', 'container-fluid' => 'Container Fluid' ),
				'default'  => 'container'
			),

			array(
				'id'       => 'single_blog_sidebar',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Sidebar', 'hemelios' ),
				'subtitle' => esc_html__( 'Set sidebar style.', 'hemelios' ),
				'desc'     => '',
				'options'  => array(
					'none'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-none.png' ),
					'left'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-left.png' ),
					'right' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-right.png' ),
					'both'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-both.png' ),
				),
				'default'  => 'left'
			),

			array(
				'id'       => 'single_blog_sidebar_width',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Sidebar Width', 'hemelios' ),
				'subtitle' => esc_html__( 'Set sidebar width.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'small' => 'Small (1/4)', 'large' => 'Large (1/3)' ),
				'default'  => 'small',
				'required' => array( 'single_blog_sidebar', '=', array( 'left', 'both', 'right' ) ),
			),


			array(
				'id'       => 'single_blog_left_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Left Sidebar', 'hemelios' ),
				'subtitle' => "Choose the default left sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'sidebar-1',
				'required' => array( 'single_blog_sidebar', '=', array( 'left', 'both' ) ),
			),

			array(
				'id'       => 'single_blog_right_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Right Sidebar', 'hemelios' ),
				'subtitle' => "Choose the default right sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'sidebar-2',
				'required' => array( 'single_blog_sidebar', '=', array( 'right', 'both' ) ),
			),


			array(
				'id'       => 'show_single_blog_title',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Show Single Blog Title', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable single blog title.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),


			array(
				'id'       => 'single_blog_title_height',
				'type'     => 'dimensions',
				'title'    => esc_html__( 'Single Blog Title Height', 'hemelios' ),
				'desc'     => esc_html__( 'You can set a height for the single blog title here.', 'hemelios' ),
				'required' => array( 'show_single_blog_title', '=', array( '1' ) ),
				'units'    => 'px',
				'width'    => false,
				'default'  => array(
					'height' => '300'
				)
			),

			array(
				'id'       => 'breadcrumbs_in_single_blog_title',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Single Blog Breadcrumbs', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable breadcrumbs in single blog title.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'required' => array( 'show_single_blog_title', '=', array( '1' ) ),
				'default'  => '1'
			),

			array(
				'id'       => 'single_blog_breadcrumbs_position',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Single Blog Breadcrumbs Positions', 'hemelios' ),
				'subtitle' => esc_html__( 'Select single blog breadcrumbs positions.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '0' => 'Left', '1' => 'Center', '2' => 'Right' ),
				'required' => array( 'breadcrumbs_in_single_blog_title', '=', array( '1' ) ),
				'default'  => '0'
			),

			array(
				'id'       => 'single_blog_title_bg_image',
				'type'     => 'media',
				'url'      => true,
				'title'    => esc_html__( 'Single Blog Title Background', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload single blog title background.', 'hemelios' ),
				'desc'     => '',
				'default'  => array(
					'url' => $archive_title_bg_url
				)
			),
		)
	)
);