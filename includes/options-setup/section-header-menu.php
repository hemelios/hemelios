<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'title'      => esc_html__( 'Header Menu', 'hemelios' ),
		'desc'       => '',
		'icon'       => 'el el-lines',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'          => 'menu_font',
				'type'        => 'typography',
				'title'       => esc_html__( 'Menu Font', 'hemelios' ),
				'subtitle'    => esc_html__( 'Select the menu font.', 'hemelios' ),
				'google'      => true,
				'all_styles'  => true, // Enable all Google Font style/weight variations to be added to the page
				'line-height' => false,
				'color'       => false,
				'text-align'  => false,
				'font-style'  => false,
				'subsets'     => false,
				'font-size'   => false,
				'font-weight' => false,
				'output'      => array( '' ), // An array of CSS selectors to apply this font style to dynamically
				'compiler'    => array( '' ), // An array of CSS selectors to apply this font style to dynamically
				'units'       => 'px', // Defaults to px
				'default'     => array(
					'font-family' => 'Poppins',
				),
			),

			array(
				'id'   => 'header_menu_divide_1',
				'type' => 'divide'
			),

			array(
				'id'       => 'menu_text_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Menu Text Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set menu text color.', 'hemelios' ),
				'default'  => '#111',
				'validate' => 'color',
			),

			array(
				'id'       => 'menu_text_hover_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Menu Text Hover Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set menu text hover color.', 'hemelios' ),
				'default'  => '#0cb4ce',
				'validate' => 'color',
			),

			array(
				'id'       => 'menu_bg_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Menu Background Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set menu background color.', 'hemelios' ),
				'default'  => 'transparent',
				'validate' => 'color',
			),

//					array(
//						'id'       => 'menu_text_bg_hover_color',
//						'type'     => 'color',
//						'title'    => esc_html__( 'Menu Text Background Hover Color', 'hemelios' ),
//						'subtitle' => esc_html__( 'Set menu text element background hover color.', 'hemelios' ),
//						'default'  => 'transparent',
//						'validate' => 'color',
//					),

			array(
				'id'   => 'header_menu_divide_2',
				'type' => 'divide'
			),

			array(
				'id'       => 'menu_sub_text_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Sub Menu Text Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set sub menu text color.', 'hemelios' ),
				'default'  => '#AAAAAA',
				'validate' => 'color',
			),

			array(
				'id'       => 'menu_sub_text_hover_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Sub Menu Text Hover Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set sub menu text hover color.', 'hemelios' ),
				'default'  => '#ffffff',
				'validate' => 'color',
			),

			array(
				'id'       => 'menu_sub_bg_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Sub Menu Background Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set sub menu background color.', 'hemelios' ),
				'default'  => '#111111',
				'validate' => 'color',
			),

			array(
				'id'       => 'menu_sub_bg_hover_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Sub Menu Text Background Hover Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set sub menu text element background hover color.', 'hemelios' ),
				'default'  => '#222222',
				'validate' => 'color',
			),
		)
	) );