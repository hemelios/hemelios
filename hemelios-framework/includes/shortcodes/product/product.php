<?php
if ( ! defined( 'ABSPATH' ) ) die( '-1' );

if ( ! defined( 'HEMELIOS_PRODUCT_CATEGORY_TAXONOMY' ) )
	define( 'HEMELIOS_PRODUCT_CATEGORY_TAXONOMY', 'products-category');

if ( ! defined( 'HEMELIOS_PRODUCT_POST_TYPE' ) )
	define( 'HEMELIOS_PRODUCT_POST_TYPE', 'products');

if(! defined( 'HEMELIOS_PRODUCT_DIR_PATH' ))
	define( 'HEMELIOS_PRODUCT_DIR_PATH', PLUGIN_HEMELIOS_FRAMEWORK_DIR .'includes/shortcodes/product');

if(!class_exists('HemeliosFramework_Products')){
	class HemeliosFramework_Products {
		function __construct() {
			add_action('wp_enqueue_scripts',array($this,'front_scripts'),11);
			add_action( 'init', array($this, 'register_taxonomies' ), 5 );
			add_action( 'init', array($this, 'register_post_types' ), 6 );
			add_shortcode('hemeliosframework_products', array($this, 'products_shortcode' ));
			add_filter( 'rwmb_meta_boxes', array($this,'register_meta_boxes' ));
			add_filter('single_template',array($this,'get_product_single_template' ) );
			add_filter('archive_template',array($this,'get_product_archive_template' ) );

			if(is_admin()){
				add_filter('manage_edit-'.HEMELIOS_PRODUCT_POST_TYPE.'_columns' , array($this,'add_products_columns'));
				add_action( 'manage_'.HEMELIOS_PRODUCT_POST_TYPE.'_posts_custom_column' ,array($this,'set_products_columns_value'), 10, 2 );
				add_action('restrict_manage_posts',array($this,'products_manage_posts'));
				add_filter('parse_query',array($this,'convert_taxonomy_term_in_query'));
				add_action('admin_menu', array($this, 'addMenuSetting'));
			}
//            $this->includes();

		}

		function front_scripts(){
			global $hemelios_options;
		}

		function register_post_types() {

			$post_type = HEMELIOS_PRODUCT_POST_TYPE;

			if (post_type_exists($post_type)) {
				return;
			}

			$post_type_slug = get_option('hemelios-hemelios-' . $post_type . '-config');
			if (!isset($post_type_slug) || !is_array($post_type_slug)) {
				$slug = 'products';
				$name = $singular_name = 'Product';
			} else {
				$slug = $post_type_slug['slug'];
				$name = $post_type_slug['name'];
				$singular_name = $post_type_slug['singular_name'];
			}

			register_post_type($post_type,
				array(
					'label' => __('Products', 'hemelios'),
					'description' => __('Products Description', 'hemelios'),
					'labels' => array(
						'name' => $name,
						'singular_name' => $singular_name,
						'menu_name' => __($name, 'hemelios'),
						'parent_item_colon' => __('Parent Item:', 'hemelios'),
						'all_items' => __(sprintf('All %s', $name), 'hemelios'),
						'view_item' => __('View Item', 'hemelios'),
						'add_new_item' => __(sprintf('Add New  %s', $name), 'hemelios'),
						'add_new' => __('Add New', 'hemelios'),
						'edit_item' => __('Edit Item', 'hemelios'),
						'update_item' => __('Update Item', 'hemelios'),
						'search_items' => __('Search Item', 'hemelios'),
						'not_found' => __('Not found', 'hemelios'),
						'not_found_in_trash' => __('Not found in Trash', 'hemelios'),
					),
					'supports' => array('title', 'editor', 'thumbnail'),
					'public' => true,
					'show_ui' => true,
					'_builtin' => false,
					'has_archive' => true,
					'menu_icon' => 'dashicons-phone',
					'rewrite' => array('slug' => $slug, 'with_front' => true),
				)
			);
			flush_rewrite_rules();

		}

		function register_taxonomies(){

			if (taxonomy_exists(HEMELIOS_PRODUCT_CATEGORY_TAXONOMY)) {
				return;
			}

			$post_type = HEMELIOS_PRODUCT_POST_TYPE;
			$taxonomy_slug = HEMELIOS_PRODUCT_CATEGORY_TAXONOMY;
			$taxonomy_name = 'Product Categories';

			$post_type_slug = get_option('hemelios-hemelios-' . $post_type . '-config');
			if (isset($post_type_slug) && is_array($post_type_slug) &&
				array_key_exists('taxonomy_slug', $post_type_slug) && $post_type_slug['taxonomy_slug'] != ''
			) {
				$taxonomy_slug = $post_type_slug['taxonomy_slug'];
				$taxonomy_name = $post_type_slug['taxonomy_name'];
			}
			register_taxonomy(HEMELIOS_PRODUCT_CATEGORY_TAXONOMY, HEMELIOS_PRODUCT_POST_TYPE,
				array('hierarchical' => true,
					  'label' => $taxonomy_name,
					  'query_var' => true,
					  'rewrite' => array('slug' => $taxonomy_slug))
			);
			flush_rewrite_rules();
		}
		function register_meta_boxes($meta_boxes){
			$meta_boxes[] = array(
				'title'  => __( 'Products Extra', 'hemelios' ),
				'id'     => 'hemelios-meta-box-product-format',
				'pages'  => array( HEMELIOS_PRODUCT_POST_TYPE ),
				'fields' => array(
					array(
						'name' => __( 'Mô tả ngắn', 'hemelios' ),
						'id'   => 'product_short_description',
						'type' => 'textarea',
					),
					array(
						'name' => esc_html__( 'Gallery', 'hemelios' ),
						'id'   => 'product_format_gallery',
						'type' => 'image_advanced',
						'desc' => esc_html__( 'Chọn hình ảnh (Nhấn Ctrl để chọn nhiều hình ảnh)', 'hemelios' )
					),
					array(
						'name' => esc_html__( 'Catalogue', 'hemelios' ),
						'id'   => 'file_download',
						'type' => 'file_advanced',
						'desc' => esc_html__( 'Chọn file Download', 'hemelios' )
					),
				)
			);
			return $meta_boxes;
		}
		function add_products_columns($columns) {
			unset(
				$columns['cb']
//				$columns['title']
//				$columns['date']
			);
			$cols = array_merge(array('cb'=>(' ')),$columns);
			$cols = array_merge($cols,array('title'=>__('Product Name','hemelios')));
//			$cols = array_merge($cols,array('thumbnail'=>__('Thumbnail','hemelios')));
			$cols = array_merge($cols,array(HEMELIOS_PRODUCT_CATEGORY_TAXONOMY=>__('Categories','hemelios')));
			$cols = array_merge($cols,array('date'=>__('Date','hemelios')));
			return $cols;
		}

		function set_products_columns_value( $column, $post_id ) {
			switch($column){
				case 'id':{
					echo wp_kses_post($post_id);
					break;
				}
				case 'thumbnail':
				{
					echo get_the_post_thumbnail($post_id,'thumbnail');
					break;
				}
				case HEMELIOS_PRODUCT_CATEGORY_TAXONOMY:
				{
					$terms = wp_get_post_terms( get_the_ID(), array( HEMELIOS_PRODUCT_CATEGORY_TAXONOMY));
					$cat = '<ul>';
					foreach ( $terms as $term ){
						$cat .= '<li><a href="'.get_term_link( $term, HEMELIOS_PRODUCT_CATEGORY_TAXONOMY ).'">'.$term->name.'<a/></li>';
					}
					$cat .= '</ul>';
					echo wp_kses_post($cat);
					break;
				}
			}
		}

		function products_manage_posts() {
			global $typenow;
			if ($typenow==HEMELIOS_PRODUCT_POST_TYPE){
				$selected = isset($_GET[HEMELIOS_PRODUCT_CATEGORY_TAXONOMY]) ? $_GET[HEMELIOS_PRODUCT_CATEGORY_TAXONOMY] : '';
				$args = array(
					'show_count' => true,
					'show_option_all' => __('Show All Categories','hemelios'),
					'taxonomy'        => HEMELIOS_PRODUCT_CATEGORY_TAXONOMY,
					'name'               => HEMELIOS_PRODUCT_CATEGORY_TAXONOMY,
					'selected' => $selected,

				);
				wp_dropdown_categories($args);
			}
		}
		function products_shortcode($atts){
			$title = $title_style = $show_readmore = $class_col_wrap = $class_col = $title = $order = $overlay_style = $view_all_link = $style = $category_style = $offset = $overlay_style = $bg_title_float_style = $current_page = $show_pagging = $show_category = $category = $column = $item = $padding = $layout_type = $el_class = $hemelios_animation = $css_animation = $duration = $delay = $styles_animation = '';
			extract( shortcode_atts( array(
				'style' => 'style_1',
				'title'   => '',
				'title_style'   => 'border-bottom',
				'style_header' => 'dark',
				'category'     => '',
				'column'  => '2',
				'item' => '3',
				'order' =>'DESC',
				'schema_style' => '',
				'show_readmore' => '',
				'el_class'      => '',
				'css_animation' => '',
				'duration'      => '',
				'delay'         => '',
				'current_page' => '1'
			), $atts ) );

			$hemelios_animation .= ' ' . esc_attr($el_class);
			$hemelios_animation .=  hemeliosFramework_Shortcodes::hemelios_get_css_animation( $css_animation );
			$styles_animation= hemeliosFramework_Shortcodes::hemelios_get_style_animation($duration,$delay);

			if( $item == '' ){
				$item = -1;
			}
			$class_col_wrap = 'col-'.$column.'';
			$query['posts_per_page'] = $item;
			$query['no_found_rows'] = true;
			$query['post_status'] = 'publish';
			$query['post_type'] = HEMELIOS_PRODUCT_POST_TYPE;
			if ( $category ) {
				$query['tax_query'] = array(
					array(
						'taxonomy' => HEMELIOS_PRODUCT_CATEGORY_TAXONOMY,
						'field'    => 'term_id',
						'terms'    =>  $category,
						//'operator' => 'LIKE'
					)
				);
			}
			if ($order == 'DESC') {
				$query['order'] = 'DESC';
			} elseif ($order == 'ASC') {
				$query['order'] = 'ASC';
			}
			$r = new WP_Query($query);
			if( $title_style == 'border-bottom' ){
				$style_header = 'title-border';
			}else{
				$style_header = 'title-no-border';
			}
			ob_start();
			$class_col = 'col-lg-' . (12 / esc_attr($column)) . ' col-md-' . (12 / esc_attr($column)) . ' col-sm-3  col-xs-12';
			if ($r->have_posts()) :?>
				<div class="os-product <?php echo esc_attr($style) ?> <?php echo esc_attr($class_col_wrap) ?> <?php echo esc_attr($style_header) ?> <?php echo esc_attr($hemelios_animation) ?>" <?php echo esc_attr($styles_animation) ?>>
					<?php if( $title !='' ) { ?>
						<h3 class="os-title title-larger"><?php echo esc_html( $title ) ?></h3>
					<?php } ?>
					<div class="row">
						<?php while ($r->have_posts()) : $r->the_post(); ?>

							<div class="<?php echo esc_attr($class_col); ?>">
								<div class="os-product-item">
									<?php if( $style == 'style_1' ) : ?>
										<h4 class="os-title"><?php echo esc_html(the_title()) ?></h4>
									<?php endif; ?>
									<?php
									$thumbnail_url = wp_get_attachment_url(get_post_thumbnail_id( get_the_ID() ));
									$img = '';
									if(count($thumbnail_url)>0){
										$resize = matthewruddy_image_resize($thumbnail_url,370,247);
										if($resize!=null )
											$img = $resize['url'];
									}
									if( $img ){ ?>
										<div class="os-product-thumb">
											<img src="<?php echo esc_attr($img) ?>" alt="product" />
											<div class="hover-action">
												<a href="<?php the_permalink() ?>"><i class="fa fa-link"></i></a>
											</div>
										</div>
									<?php 	}
									?>
									<div class="os-product-content">
										<?php if( $style == 'style_2' ) : ?>
											<h4 class="os-title"><?php echo esc_html(the_title()) ?></h4>
										<?php endif; ?>
										<div class="product-excerpt">
											<p><?php echo esc_html( get_post_meta(get_the_ID(),'product_short_description',true) ); ?></p>
										</div>
										<?php if( $show_readmore == 'yes' ) : ?>
											<div class="readmore-wrapper">
												<a class="product-readmore" href="<?php the_permalink() ?>" title="<?php echo esc_html(the_title()) ?>">
													<?php echo __('Read more','hemelios') ?>
												</a>
											</div>
										<?php endif; ?>
									</div>
								</div>
							</div>

						<?php endwhile; ?>
					</div>
				</div>
				<?php
			endif;
			$ret = ob_get_contents();
			ob_end_clean();
			return $ret;
		}

		function get_product_single_template($single) {
			global $post;
			/* Checks for single template by post type */
			if ($post->post_type == HEMELIOS_PRODUCT_POST_TYPE){
				$plugin_path =  untrailingslashit( HEMELIOS_PRODUCT_DIR_PATH );
				$template_path = $plugin_path . '/templates/single/single-product.php';
				if(file_exists($template_path))
					return $template_path;
			}
			return $single;
		}
		function get_product_archive_template($archive_template) {
			global $post;
			/* Checks for archive template by post type */
			//if ( is_post_type_archive ( 'products' ) ) {
				$plugin_path =  untrailingslashit( HEMELIOS_PRODUCT_DIR_PATH );
				$template_path = $plugin_path . '/templates/archive/archive-product.php';
				if(file_exists($template_path))
					return $template_path;
			//
			//
			return $archive_template;
		}



		function product_manage_posts() {
			global $typenow;
			if ($typenow==HEMELIOS_PRODUCT_POST_TYPE){
				$selected = isset($_GET[HEMELIOS_PRODUCT_CATEGORY_TAXONOMY]) ? $_GET[HEMELIOS_PRODUCT_CATEGORY_TAXONOMY] : '';
				$args = array(
					'show_count' => true,
					'show_option_all' => __('Show All Categories','hemelios'),
					'taxonomy'        => HEMELIOS_PRODUCT_CATEGORY_TAXONOMY,
					'name'               => HEMELIOS_PRODUCT_CATEGORY_TAXONOMY,
					'selected' => $selected,

				);
				wp_dropdown_categories($args);
			}
		}

		function convert_taxonomy_term_in_query($query) {
			global $pagenow;
			$qv = &$query->query_vars;
			if ($pagenow=='edit.php' &&
				isset($qv[HEMELIOS_PRODUCT_CATEGORY_TAXONOMY])  &&
				is_numeric($qv[HEMELIOS_PRODUCT_CATEGORY_TAXONOMY])) {
				$term = get_term_by('id',$qv[HEMELIOS_PRODUCT_CATEGORY_TAXONOMY],HEMELIOS_PRODUCT_CATEGORY_TAXONOMY);
				$qv[HEMELIOS_PRODUCT_CATEGORY_TAXONOMY] = $term->slug;
			}
		}

//        private function includes(){
//            include_once('utils/ajax-action.php');
//        }

		function addMenuSetting()
		{
			add_submenu_page('edit.php?post_type='.HEMELIOS_PRODUCT_POST_TYPE, 'Setting', 'Settings', 'edit_posts', basename(__FILE__), array($this, 'initPageSettings'));
		}

		function initPageSettings()
		{
			$template_path = ABSPATH . 'wp-content/plugins/hemelios-framework/includes/shortcodes/posttype-settings/settings.php';
			if (file_exists($template_path))
				require_once $template_path;
		}

	}
	new HemeliosFramework_Products();
}