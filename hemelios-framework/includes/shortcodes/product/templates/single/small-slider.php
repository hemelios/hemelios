<?php

do_action('hemelios_before_page');

$data_section_id = uniqid();

$terms = wp_get_post_terms( get_the_ID(), array( HEMELIOS_PORTFOLIO_CATEGORY_TAXONOMY));
$cat = $cat_filter = '';
foreach ( $terms as $term ){
	$cat_filter .= preg_replace('/\s+/', '', $term->name) .' ';
	$cat .= $term->name.', ';
}
$cat = rtrim($cat,', ');

?>
<?php
global $hemelios_options;

$layout_style = hemelios_get_post_meta($post->ID, 'hemelios_page_layout', true);
if (($layout_style === '') || ($layout_style == '-1')) {
	$layout_style = $hemelios_options['product_layout'];
}

$sidebar = hemelios_get_post_meta($post->ID, 'hemelios_page_sidebar', true);
if (($sidebar === '') || ($sidebar == '-1')) {
	$sidebar = $hemelios_options['product_sidebar'];
}

$left_sidebar = hemelios_get_post_meta($post->ID, 'hemelios_page_left_sidebar', true);
if (($left_sidebar === '') || ($left_sidebar == '-1')) {
	$left_sidebar = $hemelios_options['product_left_sidebar'];

}

$right_sidebar = hemelios_get_post_meta($post->ID, 'hemelios_page_right_sidebar', true);
if (($right_sidebar === '') || ($right_sidebar == '-1')) {
	$right_sidebar = $hemelios_options['product_right_sidebar'];
}

$sidebar_width = hemelios_get_post_meta($post->ID, 'hemelios_sidebar_width', true);
if (($sidebar_width === '') || ($sidebar_width == '-1')) {
	$sidebar_width = $hemelios_options['product_sidebar_width'];
}

// Calculate sidebar column & content column
$sidebar_col = 'col-md-3';
if ($sidebar_width == 'large') {
	$sidebar_col = 'col-md-4';
}

$content_col_number = 12;
if (is_active_sidebar($left_sidebar) && (($sidebar == 'both') || ($sidebar == 'left'))) {
	if ($sidebar_width == 'large') {
		$content_col_number -= 4;
	} else {
		$content_col_number -= 3;
	}
}
if (is_active_sidebar($right_sidebar) && (($sidebar == 'both') || ($sidebar == 'right'))) {
	if ($sidebar_width == 'large') {
		$content_col_number -= 4;
	} else {
		$content_col_number -= 3;
	}
}

$content_col = 'col-md-' . $content_col_number;
if (($content_col_number == 12) && ($layout_style == 'full')) {
	$content_col = '';
}
$main_class = array('site-content-page');

if ($content_col_number < 12) {
	$main_class[] = 'has-sidebar';
}
?>
<main class="<?php echo join(' ',$main_class) ?>">
	<?php if ($layout_style != 'full'): ?>
	<div class="<?php echo esc_attr($layout_style) ?> clearfix">
		<?php endif;?>
		<?php if (($content_col_number != 12) || ($layout_style != 'full')): ?>
		<div class="row clearfix">
			<?php endif;?>
			<?php if (is_active_sidebar( $left_sidebar ) && (($sidebar == 'left') || ($sidebar == 'both'))): ?>
				<div class="sidebar left-sidebar <?php echo esc_attr($sidebar_col) ?>">
					<?php dynamic_sidebar( $left_sidebar );?>
				</div>
			<?php endif;?>
			<div class="site-content-page-inner <?php echo esc_attr($content_col) ?>">
				<div class="page-content product-content row">
					<?php if( isset($meta_values) && !empty($meta_values) ){ ?>
					<div class="products-slideshow col-md-8" id="products_slideshow_<?php echo esc_attr($data_section_id) ?>">
						<div id="products-slider" class="portfolio-single-slider">
							<?php
							$index = 0;
							foreach($meta_values as $image){
								$urls = wp_get_attachment_image_src($image,'full');
								$img = '';
								if($urls){
									$resize = matthewruddy_image_resize($urls[0],500,375);
									if($resize!=null )
										$img = $resize['url'];
								}
								?>
							<div>
								<a href="<?php echo esc_url($img) ?>" data-index="<?php echo esc_attr($image) ?>" data-rel="prettyPhoto[portfolio-gallery]" >
									<img  alt="portfolio" src="<?php echo esc_url($img) ?>" />
								</a>
							</div>
							<?php } ?>
						</div>
						<div id="products-carousel" class="owl-carousel manual portfolio-single-slider">
							<?php
							foreach($meta_values as $image){
								$urls = wp_get_attachment_image_src($image,'full');
								$img = '';
								if($urls){
									$resize = matthewruddy_image_resize($urls[0],170,133);
									if($resize!=null )
										$img = $resize['url'];
								}
								?>
								<div class="portfolio-thumbnail">
									<a href="<?php echo esc_url($img) ?>" data-index="<?php echo esc_attr($image) ?>"  >
										<img  alt="portfolio" src="<?php echo esc_url($img) ?>" />
									</a>
								</div>
							<?php } ?>
						</div>
					</div>

						<?php
						}else {
						if(count($imgThumbs)>0) {
							$urls = $imgThumbs[0];
							$img = '';
							if($urls){
								$resize = matthewruddy_image_resize($urls,500,375);
								if($resize!=null )
									$img = $resize['url'];
							}
							?>
							<div class="item thumb-product col-md-8"><img alt="<?php the_title() ?>" src="<?php echo esc_url($img)?>" /></div>
						<?php }
						}
						?>

					<div class="col-md-4 product-info">
						<h3 class="os-title">
							<?php the_title() ?>
						</h3>
						<div class="short-descriptions">
							<?php if( $short_description ) :  ?>
							<?php echo '<p>' . wp_kses_post($short_description) .'</p>' ?>
							<?php endif; ?>
						</div>
						<div class="download">
							<?php
								if( $download ){
									foreach( $download as $key => $value ){
										$download_url = wp_get_attachment_url($value);
										$number = $key+1;
										echo '<p>'. do_shortcode('[easy_media_download url="'. $download_url .'" width="200" color="red_darker" force_dl="1" text="Download Catalogue '. $number .'"] ') .'</p>';
									}
								}
							?>
							<?php //echo do_shortcode('[easy_media_download url="http://cashbook.osthemes.biz/wp-content/uploads/2016/01/cashbook-8-870x434.jpg" width="200" color="red_darker" force_dl="1" text="Download Catalogue"] ') ?>
						</div>
						<div class="social-share">
							<?php
							$hemelios_options = hemelios_option();
							$sharing_facebook  = isset( $hemelios_options['social_sharing']['facebook'] ) ? $hemelios_options['social_sharing']['facebook'] : 0;
							$sharing_twitter   = isset( $hemelios_options['social_sharing']['twitter'] ) ? $hemelios_options['social_sharing']['twitter'] : 0;
							$sharing_google    = isset( $hemelios_options['social_sharing']['google'] ) ? $hemelios_options['social_sharing']['google'] : 0;
							$sharing_linkedin  = isset( $hemelios_options['social_sharing']['linkedin'] ) ? $hemelios_options['social_sharing']['linkedin'] : 0;
							$sharing_tumblr    = isset( $hemelios_options['social_sharing']['tumblr'] ) ? $hemelios_options['social_sharing']['tumblr'] : 0;
							$sharing_pinterest = isset( $hemelios_options['social_sharing']['pinterest'] ) ? $hemelios_options['social_sharing']['pinterest'] : 0;
							if ( ( $sharing_facebook == 1 ) ||
								( $sharing_twitter == 1 ) ||
								( $sharing_linkedin == 1 ) ||
								( $sharing_tumblr == 1 ) ||
								( $sharing_google == 1 ) ||
								( $sharing_pinterest == 1 )
							) :
								?>
								<div class="social-share-wrap">

									<ul class="social-share show">
										<?php if ( $sharing_facebook == 1 ) : ?>
											<li>
												<a onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_attr( urlencode( get_permalink() ) ); ?>','Share', 'toolbar=0,status=0,width=620,height=280');" data-toggle="tooltip" title="<?php echo esc_html__( 'Share on Facebook', 'hemelios' ); ?>" href="javascript:">
													<i class="fa fa-facebook"></i>
												</a>
											</li>
										<?php endif; ?>

										<?php if ( $sharing_twitter == 1 ) : ?>
											<li>
												<a onclick="popUp=window.open('http://twitter.com/home?status=<?php echo esc_attr( urlencode( get_the_title() ) ); ?> <?php echo esc_attr( urlencode( get_permalink() ) ); ?>','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;" data-toggle="tooltip" title="<?php echo esc_html__( 'Share on Twitter', 'hemelios' ); ?>" href="javascript:;">
													<i class="fa fa-twitter"></i>
												</a>
											</li>
										<?php endif; ?>

										<?php if ( $sharing_google == 1 ) : ?>
											<li>
												<a data-toggle="tooltip" title="<?php echo esc_html__( 'Share on Google +', 'hemelios' ); ?>" href="javascript:;" onclick="popUp=window.open('https://plus.google.com/share?url=<?php echo esc_attr( urlencode( get_permalink() ) ); ?>','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;">
													<i class="fa fa-google-plus"></i>
												</a>
											</li>
										<?php endif; ?>

										<?php if ( $sharing_linkedin == 1 ): ?>
											<li>
												<a data-toggle="tooltip" title="<?php echo esc_html__( 'Share on Linkedin', 'hemelios' ); ?>" onclick="popUp=window.open('http://linkedin.com/shareArticle?mini=true&amp;url=<?php echo esc_attr( urlencode( get_permalink() ) ); ?>&amp;title=<?php echo esc_attr( urlencode( get_the_title() ) ); ?>','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;" href="javascript:;">
													<i class="fa fa-linkedin"></i>
												</a>
											</li>
										<?php endif; ?>

										<?php if ( $sharing_tumblr == 1 ) : ?>
											<li>
												<a data-toggle="tooltip" title="<?php echo esc_html__( 'Share on Tumblr', 'hemelios' ); ?>" onclick="popUp=window.open('http://www.tumblr.com/share/link?url=<?php echo esc_attr( urlencode( get_permalink() ) ); ?>&amp;name=<?php echo esc_attr( urlencode( get_the_title() ) ); ?>&amp;description=<?php echo esc_attr( urlencode( get_the_excerpt() ) ); ?>','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;" href="javascript:;">
													<i class="fa fa-tumblr"></i>
												</a>
											</li>

										<?php endif; ?>

										<?php if ( $sharing_pinterest == 1 ) : ?>
											<li>
												<a data-toggle="tooltip" title="<?php echo esc_html__( 'Share on Pinterest', 'hemelios' ); ?>" onclick="popUp=window.open('http://pinterest.com/pin/create/button/?url=<?php echo esc_attr( urlencode( get_permalink() ) ); ?>&amp;description=<?php echo esc_attr( urlencode( get_the_title() ) ); ?>&amp;media=<?php $arrImages = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
												echo has_post_thumbnail() ? esc_attr( $arrImages[0] ) : ""; ?>','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;" href="javascript:;">
													<i class="fa fa-pinterest"></i>
												</a>
											</li>
										<?php endif; ?>
									</ul>
								</div>
							<?php endif;
							?>
						</div>
					</div>

				</div>
				<div class="product-entry-content">
					<h3 class="os-title"><?php echo esc_html__('Mô tả sản phẩm','hemelios') ?></h3>
					<?php the_content() ?>
				</div>
			</div>
			<?php if (is_active_sidebar( $right_sidebar ) && (($sidebar == 'right') || ($sidebar == 'both'))): ?>
				<div class="sidebar right-sidebar <?php echo esc_attr($sidebar_col) ?>">
					<?php dynamic_sidebar( $right_sidebar );?>
				</div>
			<?php endif;?>
			<?php if (($content_col_number != 12) || ($layout_style != 'full')): ?>
		</div>
	<?php endif;?>
		<?php if ($layout_style != 'full'): ?>
	</div>
<?php endif;?>

</main>
<?php
if( is_active_sidebar('sidebar-products-bottom') ){
	?>
	<div class="bottom-single-product sidebar">
		<?php dynamic_sidebar('sidebar-products-bottom'); ?>
	</div>
	<?php
}
?>


<div class="related-products">
	<div class="container">
		<h2 class="os-title"><?php echo esc_html__( 'Sản phẩm liên quan', 'hemelios' ) ?></h2>
		<div class="site-content-archive-inner blog-inner product-inner-wrap">
			<div class="related-wrapper row">
				<?php
				wp_reset_query();
				$term_list = wp_get_post_terms( $post->ID, 'products-category', array( "fields" => "ids" ) );
				if ( $term_list ) {
					$term_ids = array();
					foreach ( $term_list as $individual_category ) {
						$term_ids[] = $individual_category;
					}
					$args = array(
						'post__not_in'     => array( $post->ID ),
						'posts_per_page'   => 4, // Number of related posts that will be displayed.
						'caller_get_posts' => 1,
						'orderby'          => 'rand', // Randomize the posts
						'tax_query'        => array(
							array(
								'taxonomy' => 'products-category',
								'field'    => 'term_id',
								'terms'    => $term_ids
							)
						)
					);

					query_posts( $args );
					$class   = array();
					$class[] = 'col-md-3 col-sm-6 col-xs-12';
					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
							?>
							<article id="post-<?php the_ID(); ?>" <?php post_class( $class ); ?>>
								<div class="entry-wrap">
									<?php
									$thumbnail = hemelios_post_thumbnail( $size );
									if ( !empty( $thumbnail ) ) : ?>
										<div class="entry-thumbnail-wrap">
											<?php echo wp_kses_post( $thumbnail ); ?>
										</div>
									<?php endif; ?>
									<div class="entry-content-wrap">
										<div class="entry-content-top-wrap clearfix">
											<div class="entry-content-top-right">
												<h3 class="entry-title">
													<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
												</h3>
												<div class="entry-post-meta-wrap">
													<?php hemelios_post_meta(); ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</article>
							<?php
						endwhile;
						wp_reset_postdata();
						wp_reset_query();
					endif;
				}
				?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	(function ($) {
		"use strict";
		$(document).ready(function () {

			var sync1 = $("#products-slider", ".products-slideshow");
			var sync2 = $("#products-carousel", ".products-slideshow");

			sync1.owlCarousel({
				singleItem           : true,
				slideSpeed           : 100,
				navigation           : true,
				navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
				pagination           : false,
				afterAction          : syncPosition,
				responsiveRefreshRate: 200
			});

			sync2.owlCarousel({
				items                : 4,
				itemsDesktop         : [1199, 4],
				itemsDesktopSmall    : [980, 4],
				itemsTablet          : [768, 4],
				itemsTabletSmall     : false,
				itemsMobile          : [479, 2],
				pagination           : false,
				responsiveRefreshRate: 100,
				navigation           : false,
				//navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
				afterInit            : function (el) {
					el.find(".owl-item").eq(0).addClass("synced");
				},
				afterMove: function (elem) {
					var current = this.currentItem;
					var src = elem.find(".owl-item").eq(current).find("img").attr('src');
					console.log('Image current is ' + src);
				}
			});
			function syncPosition(el) {
				var current = this.currentItem;
				$("#products-carousel")
						.find(".owl-item")
						.removeClass("synced")
						.eq(current)
						.addClass("synced");
				if ($("#products-carousel").data("owlCarousel") !== undefined) {
				}
			}

			$("#products-carousel").on("click", ".owl-item", function (e) {
				e.preventDefault();
				var number = $(this).data("owlItem");

				sync1.trigger("owl.goTo", number);
			});

			function center(number) {
				var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
				var num = number;
				var found = false;
				for (var i in sync2visible) {
					if (num === sync2visible[i]) {
						var found = true;
					}
				}

				if (found === false) {
					if (num > sync2visible[sync2visible.length - 1]) {
						sync2.trigger("owl.goTo", num - sync2visible.length + 2)
					} else {
						if (num - 1 === -1) {
							num = 0;
						}
						sync2.trigger("owl.goTo", num);
					}
				} else if (num === sync2visible[sync2visible.length - 1]) {
					sync2.trigger("owl.goTo", sync2visible[1])
				} else if (num === sync2visible[0]) {
					sync2.trigger("owl.goTo", num - 1)
				}
			}
		});
	})(jQuery);
</script>