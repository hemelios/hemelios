<?php
get_header();
if ( have_posts() ) {
	// Start the Loop.
	while ( have_posts() ) : the_post();
		$post_id = get_the_ID();

		$meta_values       = hemelios_get_post_meta( get_the_ID(), 'product_format_gallery', false );
		$short_description = hemelios_get_post_meta( get_the_ID(), 'product_short_description', true );
		$download          = hemelios_get_post_meta( get_the_ID(), 'file_download', false );
		$imgThumbs         = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'full' );
		$cat               = '';
		$arrCatId          = array();

		$image_size = 'thumbnail-1170x730'; // image size for related

		include_once( PLUGIN_HEMELIOS_FRAMEWORK_DIR . 'includes/shortcodes/product/templates/single/small-slider.php' );

	endwhile;
}
?>

<script type="text/javascript">
	//	(function ($) {
	//		"use strict";
	//		$(document).ready(function () {
	//			$("a[rel^='prettyPhoto']").prettyPhoto(
	//				{
	//					theme       : 'light_rounded',
	//					slideshow   : 5000,
	//					deeplinking : false,
	//					social_tools: false
	//				});
	//			$('.portfolio-item > div.entry-thumbnail').hoverdir();
	//		});
	//
	//
	//
	//
	//	})(jQuery);
</script>

<?php get_footer(); ?>
