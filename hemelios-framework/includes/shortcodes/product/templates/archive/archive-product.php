<?php
get_header();
$hemelios_options      = hemelios_option();
$hemelios_archive_loop = hemelios_archive_loop();

$layout_style = isset( $_GET['layout'] ) ? $_GET['layout'] : '';
if ( !in_array( $layout_style, array( 'full', 'container', 'container-fluid' ) ) ) {
	$layout_style = $hemelios_options['product_layout'];
}

$sidebar = isset( $_GET['sidebar'] ) ? $_GET['sidebar'] : '';
if ( !in_array( $sidebar, array( 'none', 'left', 'right', 'both' ) ) ) {
	$sidebar = $hemelios_options['product_sidebar'];
}

$archive_paging_style = isset( $_GET['paging'] ) ? $_GET['paging'] : '';
if ( !in_array( $archive_paging_style, array( 'default', 'load-more', 'infinity-scroll' ) ) ) {
	$archive_paging_style = $hemelios_options['product_paging_style'];
}

$sidebar_width = isset( $_GET['sidebar_width'] ) ? $_GET['sidebar_width'] : '';
if ( !in_array( $sidebar_width, array( 'small', 'large' ) ) ) {
	$sidebar_width = $hemelios_options['product_sidebar_width'];
}

$left_sidebar  = $hemelios_options['product_left_sidebar'];
$right_sidebar = $hemelios_options['product_right_sidebar'];

$sidebar_col = 'col-md-3';
if ( $sidebar_width == 'large' ) {
	$sidebar_col = 'col-md-4';
}
$hemelios_archive_loop['image-size'] = 'blog-classic';
$content_col_number                  = 12;
if ( is_active_sidebar( $left_sidebar ) && ( ( $sidebar == 'both' ) || ( $sidebar == 'left' ) ) ) {
	if ( $sidebar_width == 'large' ) {
		$content_col_number -= 4;
	} else {
		$content_col_number -= 3;
	}
	$hemelios_archive_loop['image-size'] = 'blog-classic-sidebar';
}
if ( is_active_sidebar( $right_sidebar ) && ( ( $sidebar == 'both' ) || ( $sidebar == 'right' ) ) ) {
	if ( $sidebar_width == 'large' ) {
		$content_col_number -= 4;
	} else {
		$content_col_number -= 3;
	}
	$hemelios_archive_loop['image-size'] = 'blog-classic-sidebar';
}

$content_col = 'col-md-' . $content_col_number;
if ( ( $content_col_number == 12 ) && ( $layout_style == 'full' ) ) {
	$content_col = '';
}

$blog_class = array( 'blog-inner product-inner-wrap', 'clearfix' );

$blog_wrap_class = array( 'blog-wrap' );

$blog_wrap_class[] = 'layout-' . $layout_style;

?>
<?php
/**
 * @hooked - hemelios_archive_heading - 5
 **/
do_action( 'hemelios_before_archive' );
?>
	<main class="site-content-archive">
		<?php if ( $layout_style != 'full' ): ?>
		<div class="<?php echo esc_attr( $layout_style ) ?> clearfix">
			<?php endif; ?>
			<?php if ( ( $content_col_number != 12 ) || ( $layout_style != 'full' ) ): ?>
			<div class="row clearfix">
				<?php endif; ?>
				<?php if ( is_active_sidebar( $left_sidebar ) && ( ( $sidebar == 'left' ) || ( $sidebar == 'both' ) ) ): ?>
					<div class="sidebar left-sidebar <?php echo esc_attr( $sidebar_col ) ?> hidden-sm hidden-xs">
						<?php dynamic_sidebar( $left_sidebar ); ?>
					</div>
				<?php endif; ?>
				<div class="site-content-archive-inner <?php echo esc_attr( $content_col ) ?>">
					<div class="<?php echo join( ' ', $blog_wrap_class ); ?>">
						<div class="<?php echo join( ' ', $blog_class ); ?>">
							<?php
							if ( have_posts() ) :
								$number = 0;
								// Start the Loop.

								$class[] = "clearfix";
								while ( have_posts() ) : the_post();
									$class   = array();
									$number ++;
									/*
									 * Include the post format-specific template for the content. If you want to
									 * use this in a child theme, then include a file called called content-___.php
									 * (where ___ is the post format) and that will be used instead.
									 */
									if( $number % 2 != 0 ) {
										$class1 = 'thumb-right';
									}else{
										$class1 = 'thumb-left';
									}

										?>
										<article id="post-<?php the_ID(); ?>" <?php post_class( $class ); ?>>
											<div class="entry-wrap row">
												<?php
												$thumbnail = hemelios_post_thumbnail( $size );
												if ( !empty( $thumbnail ) ) : ?>
													<div class="entry-thumbnail-wrap col-md-6 col-sm-6 col-xs-12 <?php echo esc_attr($class1) ?>">
														<?php echo wp_kses_post( $thumbnail ); ?>
													</div>
												<?php endif; ?>
												<div class="entry-content-wrap col-md-6 col-sm-6 col-xs-12">
													<div class="entry-content-top-wrap clearfix">
														<div class="entry-content-top-right">
															<h3 class="entry-title">
																<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
															</h3>

															<div class="entry-post-meta-wrap">
																<?php hemelios_post_meta(); ?>
															</div>
														</div>
													</div>
													<div class="entry-excerpt">
														<?php the_excerpt(); ?>
													</div>
												</div>
											</div>
										</article>



									<?php
								endwhile;
								hemelios_archive_loop_reset();
							else :
								// If no content, include the "No posts found" template.
								hemelios_get_template( 'product/content-none' );
							endif;
							?>
						</div>
						<?php
						global $wp_query;
						if ( $wp_query->max_num_pages > 1 ) :
							?>
							<div class="blog-paging-wrapper blog-paging-<?php echo esc_attr( $archive_paging_style ); ?>">
								<?php
								switch ( $archive_paging_style ) {
									case 'load-more':
										hemelios_paging_load_more();
										break;
									case 'infinity-scroll':
										hemelios_paging_infinitescroll();
										break;
									default:
										echo hemelios_paging_nav();
										break;
								}
								?>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<?php if ( is_active_sidebar( $right_sidebar ) && ( ( $sidebar == 'right' ) || ( $sidebar == 'both' ) ) ): ?>
					<div class="sidebar right-sidebar <?php echo esc_attr( $sidebar_col ) ?> hidden-sm hidden-xs">
						<?php dynamic_sidebar( $right_sidebar ); ?>
					</div>
				<?php endif; ?>
				<?php if ( ( $content_col_number != 12 ) || ( $layout_style != 'full' ) ): ?>
			</div>
		<?php endif; ?>
			<?php if ( $layout_style != 'full' ): ?>
		</div>
	<?php endif; ?>
	</main>

<?php get_footer(); ?>