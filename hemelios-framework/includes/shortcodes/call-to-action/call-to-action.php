<?php
// don't load directly
if (!defined('ABSPATH')) die('-1');
if (!class_exists('hemeliosFramework_ShortCode_Call_To_Action')) {
    class hemeliosFramework_ShortCode_Call_To_Action
    {
        function __construct()
        {
            add_shortcode('os_call_to_action', array($this, 'call_to_action_shortcode'));
        }

        function call_to_action_shortcode($atts)
        {
            $title = $link = $color = $bg_color = $el_class = $hemelios_animation = $css_animation = $duration = $delay = '';
            extract(shortcode_atts(array(
                'title'         =>'For expert financial advice you can trust HEMELIOS',
                'link'          => array( 'url' => '#', 'title' => 'Contact Us', 'target' => '' ),
                'color'         =>'',
                'bg_color'      =>'',
                'el_class'      => '',
                'css_animation' => '',
                'duration'      => '',
                'delay'         => ''
            ), $atts));
            $hemelios_animation .= ' ' . esc_attr($el_class);
            $hemelios_animation .= hemeliosFramework_Shortcodes::hemelios_get_css_animation($css_animation);
            $custom_styles = array();
            if( $color != '' ){
                $custom_styles[] = 'color: '. $color .'';
            }
            if( $bg_color != '' ){
                $custom_styles[] = 'background-color: '. $bg_color .'';
            }
            $style = '';
            if( $custom_styles ){
                $style = 'style="'. join(';',$custom_styles).'"';
            }

            if(!is_array($link)){
                $link = vc_build_link($link);
            }

            ob_start();?>
                <div class="os-call-to-action <?php echo esc_attr($el_class) ?> <?php echo esc_attr($hemelios_animation) ?>" <?php echo hemeliosFramework_Shortcodes::hemelios_get_style_animation($duration, $delay); echo $style;?>>
                    <span class="__text"><?php echo wp_kses_post($title);?></span>
                    <?php if($link['url'] != ''):?>
                        <span> - </span>
                        <a class="__link" href="<?php echo esc_attr($link['url']) ?>"  target="<?php echo esc_attr($link['target']) ?>" style="border-color:'<?php esc_html_e($color) ?>' "><?php echo wp_kses_post($link['title']) ?></a>
                    <?php endif; ?>
                </div>
            <?php
            $content = ob_get_clean();
            return $content;
        }
    }
    new hemeliosFramework_ShortCode_Call_To_Action();
}