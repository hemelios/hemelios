<?php

do_action('hemelios_before_page');

$data_section_id = uniqid();

$terms = wp_get_post_terms( get_the_ID(), array( HEMELIOS_PORTFOLIO_CATEGORY_TAXONOMY));
$cat = $cat_filter = '';
foreach ( $terms as $term ){
	$cat_filter .= preg_replace('/\s+/', '', $term->name) .' ';
	$cat .= $term->name.', ';
}
$cat = rtrim($cat,', ');
$class_project_info = '';
if( $show_project_info == '1' ){
	$class_project_info = 'col-md-9';
}else{
	$class_project_info = 'col-md-12';
}
?>
<div class="portfolio-full small-slider" id="content">
	<div class="container">
		<div class="row top-portfolio">
			<div class="<?php echo esc_attr($class_project_info) ?>">
				<?php if( isset($meta_values) && !empty($meta_values) ){ ?>
					<div class="portfolio-slideshow" id="post_slideshow_<?php echo esc_attr($data_section_id) ?>">
						<div id="portfolio-slider" class="owl-carousel manual portfolio-single-slider">
							<?php
							$index = 0;
							foreach($meta_values as $image){
								$urls = wp_get_attachment_image_src($image,'full');
								$img = '';
								if($urls){
									$resize = matthewruddy_image_resize($urls[0],870,434);
									if($resize!=null )
										$img = $resize['url'];
								}
								?>
								<div>
									<a href="<?php echo esc_url($img) ?>" data-index="<?php echo esc_attr($image) ?>" data-rel="prettyPhoto[portfolio-gallery]" >
										<img  alt="portfolio" src="<?php echo esc_url($img) ?>" />
									</a>
								</div>
							<?php } ?>
						</div>
						<div id="portfolio-carousel" class="owl-carousel manual portfolio-single-slider">
							<?php
							foreach($meta_values as $image){
								$urls = wp_get_attachment_image_src($image,'full');
								$img = '';
								if($urls){
									$resize = matthewruddy_image_resize($urls[0],170,133);
									if($resize!=null )
										$img = $resize['url'];
								}
								?>
								<div class="portfolio-thumbnail">
									<a href="<?php echo esc_url($img) ?>" data-index="<?php echo esc_attr($image) ?>"  >
										<img  alt="portfolio" src="<?php echo esc_url($img) ?>" />
									</a>
								</div>
							<?php } ?>
						</div>
					</div>
					<?php
				}elseif($imgThumbs) {?>
					<div class="item">
						<img alt="<?php the_title() ?>" src="<?php echo esc_url($imgThumbs[0])?>" /></div>
					<?php
				}
				?>


			</div>
			<?php
			if( $show_project_info == '1' ){
				?>
				<div class="col-md-3 portfolio-attribute">
					<h3 class="os-title">
						<?php echo __( 'Project info:', 'hemelios' ) ?>
					</h3>
					<div class="portfolio-info border-primary-color">
						<?php if( $short_description ) : ?>
							<div class="portfolio-short-descripton">
								<p><?php echo esc_html( $short_description ) ?></p>
							</div>
						<?php endif; ?>
						<?php if( $client ) : ?>
							<div class="portfolio-info-box">
								<h6 class="heading-font"><?php echo __( 'Client', 'hemelios' ) ?></h6>
								<div class="portfolio-term bold-color"><?php echo esc_html( $client ) ?></div>
							</div>
						<?php endif; ?>
						<?php if( $cat ) : ?>
							<div class="portfolio-info-box">
								<h6 class="heading-font"><?php echo __( 'Categories', 'hemelios' ) ?></h6>
								<div class="portfolio-term bold-color"><?php echo esc_html( $cat ); ?></div>
							</div>
						<?php endif; ?>
						<?php if( $location ) : ?>
							<div class="portfolio-info-box">
								<h6 class="heading-font"><?php echo __( 'Location', 'hemelios' ) ?></h6>
								<div class="portfolio-term bold-color"><?php echo esc_html( $location ) ?></div>
							</div>
						<?php endif; ?>
						<?php if( $area ) : ?>
							<div class="portfolio-info-box">
								<h6 class="heading-font"><?php echo __( 'Surface Area:', 'hemelios' ) ?></h6>
								<div class="portfolio-term bold-color"><?php echo wp_kses_post( $area ); ?></div>
							</div>
						<?php endif; ?>
						<?php if( $date ) : ?>
							<div class="portfolio-info-box">
								<h6 class="heading-font"><?php echo __( 'Year Completed:', 'hemelios' ) ?></h6>
								<div class="portfolio-term bold-color"><?php echo wp_kses_post( $date ); ?></div>
							</div>
						<?php endif; ?>
						<?php if( $value ) : ?>
							<div class="portfolio-info-box">
								<h6 class="heading-font"><?php echo __( 'Value:', 'hemelios' ) ?></h6>
								<div class="portfolio-term bold-color"><?php echo wp_kses_post( $value ); ?></div>
							</div>
						<?php endif; ?>

					</div>
				</div>
			<?php } ?>
		</div>
		<div class="row content-wrap">
			<?php
			$class_col = 'col-md-12';
			?>
			<div class="<?php echo esc_attr($class_col) ?> portfolio-content">
				<div class="portfolio-info">
					<?php the_content() ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	(function($) {
		"use strict";
		$(document).ready(function(){
			$('a','.portfolio-full .share').each(function(){
				$(this).click(function(){
					var href = $(this).attr('data-href');
					var leftPosition, topPosition;
					var width = 400;
					var height = 300;
					var leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
					var topPosition = (window.screen.height / 2) - ((height / 2) + 50);
					//Open the window.
					window.open(href, "", "width=300, height=200,left=" + leftPosition + ",top=" + topPosition);
				})
			})
		})
	})(jQuery)
</script>

<script type="text/javascript">
	(function ($) {
		"use strict";
		$(document).ready(function () {

			var sync1 = $("#portfolio-slider", ".portfolio-slideshow");
			var sync2 = $("#portfolio-carousel", ".portfolio-slideshow");

			sync1.owlCarousel({
				singleItem           : true,
				slideSpeed           : 100,
				navigation           : true,
				navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
				pagination           : false,
				afterAction          : syncPosition,
				responsiveRefreshRate: 200
			});

			sync2.owlCarousel({
				items                : 4,
				itemsDesktop         : [1199, 4],
				itemsDesktopSmall    : [980, 4],
				itemsTablet          : [768, 4],
				itemsTabletSmall     : false,
				itemsMobile          : [479, 2],
				pagination           : false,
				responsiveRefreshRate: 100,
				navigation           : false,
				//navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
				afterInit            : function (el) {
					el.find(".owl-item").eq(0).addClass("synced");
				},
				afterMove: function (elem) {
					var current = this.currentItem;
					var src = elem.find(".owl-item").eq(current).find("img").attr('src');
					console.log('Image current is ' + src);
				}
			});
			function syncPosition(el) {
				var current = this.currentItem;
				$("#portfolio-carousel")
						.find(".owl-item")
						.removeClass("synced")
						.eq(current)
						.addClass("synced");
				if ($("#portfolio-carousel").data("owlCarousel") !== undefined) {
				}
			}

			$("#portfolio-carousel").on("click", ".owl-item", function (e) {
				e.preventDefault();
				var number = $(this).data("owlItem");

				sync1.trigger("owl.goTo", number);
			});

			function center(number) {
				var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
				var num = number;
				var found = false;
				for (var i in sync2visible) {
					if (num === sync2visible[i]) {
						var found = true;
					}
				}

				if (found === false) {
					if (num > sync2visible[sync2visible.length - 1]) {
						sync2.trigger("owl.goTo", num - sync2visible.length + 2)
					} else {
						if (num - 1 === -1) {
							num = 0;
						}
						sync2.trigger("owl.goTo", num);
					}
				} else if (num === sync2visible[sync2visible.length - 1]) {
					sync2.trigger("owl.goTo", sync2visible[1])
				} else if (num === sync2visible[0]) {
					sync2.trigger("owl.goTo", num - 1)
				}
			}
		});
	})(jQuery);
</script>