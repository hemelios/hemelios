<?php
get_header();

if ( have_posts() ) {
    // Start the Loop.
    while ( have_posts() ) : the_post();
        $post_id = get_the_ID();

        $meta_values = hemelios_get_post_meta( get_the_ID(), 'hemelios_services_gallery', false );
        $imgThumbs = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),'full');
        $cat = '';
        $arrCatId = array();

        $image_size = 'thumbnail-1170x730'; // image size for related

        include_once(plugin_dir_path( __FILE__ ).'/small-slider.php');

    endwhile;
    }
?>

<script type="text/javascript">
	(function ($) {
		"use strict";
		$(document).ready(function () {
			$("a[rel^='prettyPhoto']").prettyPhoto(
				{
					theme       : 'light_rounded',
					slideshow   : 5000,
					deeplinking : false,
					social_tools: false
				});
			$('.portfolio-item > div.entry-thumbnail').hoverdir();
		});




	})(jQuery);
</script>

<?php get_footer(); ?>
