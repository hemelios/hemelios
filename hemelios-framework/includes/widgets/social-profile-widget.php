<?php
/**
 * Created by PhpStorm.
 * User: phuongth
 * Date: 3/26/15
 * Time: 5:24 PM
 */
class Hemelios_Social_Profile extends  Hemelios_Widget {
    public function __construct() {
        $this->widget_cssclass    = 'widget-social-profile';
        $this->widget_description = __( "Social profile widget", 'hemelios' );
        $this->widget_id          = 'hemelios-social-profile';
        $this->widget_name        = __( 'HEMELIOS - Social Profile', 'hemelios' );
        $this->settings           = array(
            'type'  => array(
                'type'    => 'select',
                'std'     => '',
                'label'   => __( 'Type', 'hemelios' ),
                'options' => array(
                    'social-icon-no-border' => __( 'No Border', 'hemelios' ),
                    'social-icon-bordered'  => __( 'Bordered', 'hemelios' )
                )
            ),
            'icons' => array(
                'type'  => 'multi-select',
                'label'   => __( 'Select social profiles', 'hemelios' ),
                'std'   => '',
	            'options' => array(
		            'twitter'  => __( 'Twitter', 'hemelios' ),
		            'facebook'  => __( 'Facebook', 'hemelios' ),
		            'dribbble'  => __( 'Dribbble', 'hemelios' ),
		            'vimeo'  => __( 'Vimeo', 'hemelios' ),
		            'tumblr'  => __( 'Tumblr', 'hemelios' ),
		            'skype'  => __( 'Skype', 'hemelios' ),
		            'linkedin'  => __( 'LinkedIn', 'hemelios' ),
		            'googleplus'  => __( 'Google+', 'hemelios' ),
		            'flickr'  => __( 'Flickr', 'hemelios' ),
		            'youtube'  => __( 'YouTube', 'hemelios' ),
		            'pinterest' => __( 'Pinterest', 'hemelios' ),
		            'foursquare'  => __( 'Foursquare', 'hemelios' ),
		            'instagram' => __( 'Instagram', 'hemelios' ),
		            'github'  => __( 'GitHub', 'hemelios' ),
		            'xing' => __( 'Xing', 'hemelios' ),
		            'behance'  => __( 'Behance', 'hemelios' ),
		            'deviantart'  => __( 'Deviantart', 'hemelios' ),
		            'soundcloud'  => __( 'SoundCloud', 'hemelios' ),
		            'yelp'  => __( 'Yelp', 'hemelios' ),
		            'rss'  => __( 'RSS Feed', 'hemelios' ),
		            'email'  => __( 'Email address', 'hemelios' ),
		            'wordpress'  => __( 'Wordpress', 'hemelios' ),
		            'stumbleupon'  => __( 'Stumbleupon', 'hemelios' ),
	            )
            )
        );
        parent::__construct();
    }

    function widget( $args, $instance ) {
        extract( $args, EXTR_SKIP );
        $type         = empty( $instance['type'] ) ? '' : apply_filters( 'widget_type', $instance['type'] );
        $icons        = empty( $instance['icons'] ) ? '' : apply_filters( 'widget_icons', $instance['icons'] );
        $class_custom = empty( $instance['class_custom'] ) ? '' : apply_filters( 'widget_class_custom', $instance['class_custom'] );
        $widget_id    = $args['widget_id'];
        global $hemelios_options;

        $twitter = '';
        if ( isset( $hemelios_options['twitter_url'] ) ) {
            $twitter = $hemelios_options['twitter_url'];
        }

	    $wordpress = '';
	    if ( isset( $hemelios_options['wordpress_url'] ) ) {
			$wordpress = $hemelios_options['wordpress_url'];
	    }
	    $stumbleupon = '';
	    if ( isset( $hemelios_options['stumbleupon_url'] ) ) {
			$stumbleupon = $hemelios_options['stumbleupon_url'];
	    }
	    $facebook = '';
	    if ( isset( $hemelios_options['facebook_url'] ) ) {
		    $facebook = $hemelios_options['facebook_url'];
	    }

	    $dribbble = '';
	    if ( isset( $hemelios_options['dribbble_url'] ) ) {
		    $dribbble = $hemelios_options['dribbble_url'];
	    }

	    $vimeo = '';
	    if ( isset( $hemelios_options['vimeo_url'] ) ) {
		    $vimeo = $hemelios_options['vimeo_url'];
	    }

	    $tumblr = '';
	    if ( isset( $hemelios_options['tumblr_url'] ) ) {
		    $tumblr = $hemelios_options['tumblr_url'];
	    }

	    $skype = $hemelios_options['skype_username'];
	    if ( isset( $hemelios_options['skype_username'] ) ) {
		    $skype = $hemelios_options['skype_username'];
	    }

	    $linkedin = '';
	    if ( isset( $hemelios_options['linkedin_url'] ) ) {
		    $linkedin = $hemelios_options['linkedin_url'];
	    }

	    $googleplus = '';
	    if ( isset( $hemelios_options['googleplus_url'] ) ) {
		    $googleplus = $hemelios_options['googleplus_url'];
	    }

	    $flickr = '';
	    if ( isset( $hemelios_options['flickr_url'] ) ) {
		    $flickr = $hemelios_options['flickr_url'];
	    }

	    $youtube = '';
	    if ( isset( $hemelios_options['youtube_url'] ) ) {
		    $youtube = $hemelios_options['youtube_url'];
	    }

	    $pinterest = '';
	    if ( isset( $hemelios_options['pinterest_url'] ) ) {
		    $pinterest = $hemelios_options['pinterest_url'];
	    }

	    $foursquare = $hemelios_options['foursquare_url'];
	    if ( isset( $hemelios_options['foursquare_url'] ) ) {
		    $foursquare = $hemelios_options['foursquare_url'];
	    }

	    $instagram = '';
	    if ( isset( $hemelios_options['instagram_url'] ) ) {
		    $instagram = $hemelios_options['instagram_url'];
	    }

	    $github = '';
	    if ( isset( $hemelios_options['github_url'] ) ) {
		    $github = $hemelios_options['github_url'];
	    }

	    $xing = $hemelios_options['xing_url'];
	    if ( isset( $hemelios_options['xing_url'] ) ) {
		    $xing = $hemelios_options['xing_url'];
	    }

	    $rss = '';
	    if ( isset( $hemelios_options['rss_url'] ) ) {
		    $rss = $hemelios_options['rss_url'];
	    }

	    $behance = '';
	    if ( isset( $hemelios_options['behance_url'] ) ) {
		    $behance = $hemelios_options['behance_url'];
	    }

	    $soundcloud = '';
	    if ( isset( $hemelios_options['soundcloud_url'] ) ) {
		    $soundcloud = $hemelios_options['soundcloud_url'];
	    }

	    $deviantart = '';
	    if ( isset( $hemelios_options['deviantart_url'] ) ) {
		    $deviantart = $hemelios_options['deviantart_url'];
	    }

	    $yelp = "";
	    if ( isset( $hemelios_options['yelp_url'] ) ) {
		    $yelp = $hemelios_options['yelp_url'];
	    }

	    $email = "";
	    if ( isset( $hemelios_options['email_address'] ) ) {
		    $email = $hemelios_options['email_address'];
	    }

	    $social_icons = '';

	    if ( empty( $icons ) ) {
		    if ( $twitter ) {
			    $social_icons .= '<li><a href="' . esc_url( $twitter ) . '" target="_blank"><i class="fa fa-twitter"></i></a></li>' . "\n";
		    }
		    if ( $facebook ) {
			    $social_icons .= '<li><a href="' . esc_url( $facebook ) . '" target="_blank"><i class="fa fa-facebook"></i></a></li>' . "\n";
		    }
		    if ( $dribbble ) {
			    $social_icons .= '<li><a href="' . esc_url( $dribbble ) . '" target="_blank"><i class="fa fa-dribbble"></i></a></li>' . "\n";
		    }
		    if ( $youtube ) {
			    $social_icons .= '<li><a href="' . esc_url( $youtube ) . '" target="_blank"><i class="fa fa-youtube"></i></a></li>' . "\n";
		    }
		    if ( $vimeo ) {
			    $social_icons .= '<li><a href="' . esc_url( $vimeo ) . '" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>' . "\n";
		    }
		    if ( $tumblr ) {
			    $social_icons .= '<li><a href="' . esc_url( $tumblr ) . '" target="_blank"><i class="fa fa-tumblr"></i></a></li>' . "\n";
		    }
		    if ( $skype ) {
			    $social_icons .= '<li><a href="skype:' . esc_attr( $skype ) . '" target="_blank"><i class="fa fa-skype"></i></a></li>' . "\n";
		    }
		    if ( $linkedin ) {
			    $social_icons .= '<li><a href="' . esc_url( $linkedin ) . '" target="_blank"><i class="fa fa-linkedin"></i></a></li>' . "\n";
		    }
		    if ( $googleplus ) {
			    $social_icons .= '<li><a href="' . esc_url( $googleplus ) . '" target="_blank"><i class="fa fa-google-plus"></i></a></li>' . "\n";
		    }
		    if ( $flickr ) {
			    $social_icons .= '<li><a href="' . esc_url( $flickr ) . '" target="_blank"><i class="fa fa-flickr"></i></a></li>' . "\n";
		    }
		    if ( $pinterest ) {
			    $social_icons .= '<li><a href="' . esc_url( $pinterest ) . '" target="_blank"><i class="fa fa-pinterest-p"></i></a></li>' . "\n";
		    }
		    if ( $foursquare ) {
			    $social_icons .= '<li><a href="' . esc_url( $foursquare ) . '" target="_blank"><i class="fa fa-foursquare"></i></a></li>' . "\n";
		    }
		    if ( $instagram ) {
			    $social_icons .= '<li><a href="' . esc_url( $instagram ) . '" target="_blank"><i class="fa fa-instagram"></i></a></li>' . "\n";
		    }
		    if ( $github ) {
			    $social_icons .= '<li><a href="' . esc_url( $github ) . '" target="_blank"><i class="fa fa-github"></i></a></li>' . "\n";
		    }
		    if ( $xing ) {
			    $social_icons .= '<li><a href="' . esc_url( $xing ) . '" target="_blank"><i class="fa fa-xing"></i></a></li>' . "\n";
		    }
		    if ( $behance ) {
			    $social_icons .= '<li><a href="' . esc_url( $behance ) . '" target="_blank"><i class="fa fa-behance"></i></a></li>' . "\n";
		    }
		    if ( $deviantart ) {
			    $social_icons .= '<li><a href="' . esc_url( $deviantart ) . '" target="_blank"><i class="fa fa-deviantart"></i></a></li>' . "\n";
		    }
		    if ( $soundcloud ) {
			    $social_icons .= '<li><a href="' . esc_url( $soundcloud ) . '" target="_blank"><i class="fa fa-soundcloud"></i></a></li>' . "\n";
		    }
		    if ( $yelp ) {
			    $social_icons .= '<li><a href="' . esc_url( $yelp ) . '" target="_blank"><i class="fa fa-yelp"></i></a></li>' . "\n";
		    }
		    if ( $rss ) {
			    $social_icons .= '<li><a href="' . esc_url( $rss ) . '" target="_blank"><i class="fa fa-rss"></i></a></li>' . "\n";
		    }
		    if ( $email ) {
			    $social_icons .= '<li><a href="mailto:' . esc_attr( $email ) . '" target="_blank"><i class="fa fa-vk"></i></a></li>' . "\n";
		    }
		    if ( $wordpress ) {
			    $social_icons .= '<li><a href="' . esc_attr( $wordpress ) . '" target="_blank"><i class="fa fa-wordpress"></i></a></li>' . "\n";
		    }
		    if ( $stumbleupon ) {
			    $social_icons .= '<li><a href="' . esc_attr( $stumbleupon ) . '" target="_blank"><i class="fa fa-stumbleupon"></i></a></li>' . "\n";
		    }
	    } else {

		    $social_type = explode( '||', $icons );
		    if (empty($wordpress)) { $wordpress = '#'; }
		    if (empty($stumbleupon)) { $stumbleupon = '#'; }
		    if (empty($twitter)) { $twitter = '#'; }
		    if (empty($facebook)) { $facebook = '#'; }
		    if (empty($dribbble)) { $dribbble = '#'; }
		    if (empty($youtube)) { $youtube = '#'; }
		    if (empty($vimeo)) { $vimeo = '#'; }
		    if (empty($tumblr)) { $tumblr = '#'; }
		    if (empty($skype)) { $skype = '#'; }
		    if (empty($linkedin)) { $linkedin = '#'; }
		    if (empty($googleplus)) { $googleplus = '#'; }
		    if (empty($flickr)) { $flickr = '#'; }
		    if (empty($pinterest)) { $pinterest = '#'; }
		    if (empty($foursquare)) { $foursquare = '#'; }
		    if (empty($instagram)) { $instagram = '#'; }
		    if (empty($github)) { $github = '#'; }
		    if (empty($xing)) { $xing = '#'; }
		    if (empty($behance)) { $behance = '#'; }
		    if (empty($deviantart)) { $deviantart = '#'; }
		    if (empty($soundcloud)) { $soundcloud = '#'; }
		    if (empty($yelp)) { $yelp = '#'; }
		    if (empty($rss)) { $rss = '#'; }
		    if (empty($email)) { $email = '#'; }

		    foreach ( $social_type as $id ) {
			    if ( ( $id == 'twitter' ) && $twitter ) {
				    $social_icons .= '<li><a href="' . esc_url( $twitter ) . '" target="_blank"><i class="fa fa-twitter"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'facebook' ) && $facebook ) {
				    $social_icons .= '<li><a href="' . esc_url( $facebook ) . '" target="_blank"><i class="fa fa-facebook"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'dribbble' ) && $dribbble ) {
				    $social_icons .= '<li><a href="' . esc_url( $dribbble ) . '" target="_blank"><i class="fa fa-dribbble"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'youtube' ) && $youtube ) {
				    $social_icons .= '<li><a href="' . esc_url( $youtube ) . '" target="_blank"><i class="fa fa-youtube"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'vimeo' ) && $vimeo ) {
				    $social_icons .= '<li><a href="' . esc_url( $vimeo ) . '" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'tumblr' ) && $tumblr ) {
				    $social_icons .= '<li><a href="' . esc_url( $tumblr ) . '" target="_blank"><i class="fa fa-tumblr"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'skype' ) && $skype ) {
				    $social_icons .= '<li><a href="skype:' . esc_attr( $skype ) . '" target="_blank"><i class="fa fa-skype"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'linkedin' ) && $linkedin ) {
				    $social_icons .= '<li><a href="' . esc_url( $linkedin ) . '" target="_blank"><i class="fa fa-linkedin"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'googleplus' ) && $googleplus ) {
				    $social_icons .= '<li><a href="' . esc_url( $googleplus ) . '" target="_blank"><i class="fa fa-google-plus"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'flickr' ) && $flickr ) {
				    $social_icons .= '<li><a href="' . esc_url( $flickr ) . '" target="_blank"><i class="fa fa-flickr"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'pinterest' ) && $pinterest ) {
				    $social_icons .= '<li><a href="' . esc_url( $pinterest ) . '" target="_blank"><i class="fa fa-pinterest-p"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'foursquare' ) && $foursquare ) {
				    $social_icons .= '<li><a href="' . esc_url( $foursquare ) . '" target="_blank"><i class="fa fa-foursquare"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'instagram' ) && $instagram ) {
				    $social_icons .= '<li><a href="' . esc_url( $instagram ) . '" target="_blank"><i class="fa fa-instagram"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'github' ) && $github ) {
				    $social_icons .= '<li><a href="' . esc_url( $github ) . '" target="_blank"><i class="fa fa-github"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'xing' ) && $xing ) {
				    $social_icons .= '<li><a href="' . esc_url( $xing ) . '" target="_blank"><i class="fa fa-xing"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'behance' ) && $behance ) {
				    $social_icons .= '<li><a href="' . esc_url( $behance ) . '" target="_blank"><i class="fa fa-behance"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'deviantart' ) && $deviantart ) {
				    $social_icons .= '<li><a href="' . esc_url( $deviantart ) . '" target="_blank"><i class="fa fa-deviantart"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'soundcloud' ) && $soundcloud ) {
				    $social_icons .= '<li><a href="' . esc_url( $soundcloud ) . '" target="_blank"><i class="fa fa-soundcloud"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'yelp' ) && $yelp ) {
				    $social_icons .= '<li><a href="' . esc_url( $yelp ) . '" target="_blank"><i class="fa fa-yelp"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'rss' ) && $rss ) {
				    $social_icons .= '<li><a href="' . esc_url( $rss ) . '" target="_blank"><i class="fa fa-rss"></i></a></li>' . "\n";
			    }
			    if ( ( $id == 'email' ) && $email ) {
				    $social_icons .= '<li><a href="mailto:' . esc_attr( $email ) . '" target="_blank"><i class="fa fa-vk"></i></a></li>' . "\n";
			    }
				if (( $id == 'wordpress' ) && $wordpress ) {
					$social_icons .= '<li><a href="' . esc_attr( $wordpress ) . '" target="_blank"><i class="fa fa-wordpress"></i></a></li>' . "\n";
				}
				if ( ( $id == 'stumbleupon' ) && $stumbleupon ) {
					$social_icons .= '<li><a href="' . esc_attr( $stumbleupon ) . '" target="_blank"><i class="fa fa-stumbleupon"></i></a></li>' . "\n";
				}
		    }
	    }


	    echo wp_kses_post( $before_widget );
	    ?>
	    <?php if ( ! empty( $class_custom ) ): ?>
		    <div class="<?php echo esc_attr( $class_custom ) ?>">
	    <?php endif; ?>
	    <ul class="widget-social-profile <?php echo esc_attr($type) ?>">
		    <?php echo wp_kses_post( $social_icons ); ?>
	    </ul>
	    <?php if ( ! empty( $class_custom ) ): ?>
		    </div>
	    <?php endif; ?>
	    <?php
	    echo wp_kses_post( $after_widget );
    }
}
if (!function_exists('hemelios_register_social_profile')) {
    function hemelios_register_social_profile() {
        register_widget('Hemelios_Social_Profile');
    }
    add_action('widgets_init', 'hemelios_register_social_profile', 1);
}