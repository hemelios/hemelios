(function($) {
    "use strict";
    var Hemelios_Install_DemoData = {
        htmlTag: {
            wrapper: '.hemelios-demo-data-wrapper'
        },
        vars: {
            is_install: false,
            try_install_count: 0,
            try_install_slider: 0
        },
        initialize: function() {
            $('.button-wrapper button', Hemelios_Install_DemoData.htmlTag.wrapper).click(function(){
                if (!confirm('Are you sure install demo data from demo site?')){
                    return;
                }
                if (Hemelios_Install_DemoData.vars.is_install) {
                    return;
                }
                $('.install-message', Hemelios_Install_DemoData.htmlTag.wrapper).removeClass('updated');
                $('.install-message', Hemelios_Install_DemoData.htmlTag.wrapper).removeClass('error');
                $('.install-message', Hemelios_Install_DemoData.htmlTag.wrapper).text('');

                Hemelios_Install_DemoData.vars.is_install = true;
                $('.install-progress-wrapper', Hemelios_Install_DemoData.htmlTag.wrapper).slideDown('fast');
                var method = $(this).attr('data-method');
                Hemelios_Install_DemoData.install('init',method, '');

                window.onbeforeunload = function(e){
                    if(!e) e = window.event;
                    e.cancelBubble = true;
                    e.returnValue = 'The install demo you made will be lost if you navigate away from this page.'; //This is displayed on the dialog

                    if (e.stopPropagation) {
                        e.stopPropagation();
                        e.preventDefault();
                    }
                };
            });
            $('#fix_install_demo_error').click(function() {
                if (!confirm('Are you sure fix demo data error?')){
                    return;
                }

                Hemelios_Install_DemoData.install('fix-data', 'livesite', '');
            });
        },
        install: function(type, method, other_data) {
            var data = {
                type: type,
                method : method,
                action: 'hemelios_install_demo',
                security: true,
                other_data: other_data
            };

            var percent = 0;
            $.ajax({
                type: 'POST',
                data: data,
                url: hemelios_install_demo_meta.ajax_url,
                success: function (data) {
                    Hemelios_Install_DemoData.vars.try_install_count = 0;
                    data = $.parseJSON(data);

                    switch (data.code) {
                        case 'error':
                        case 'fileNotFound':
                            $('.install-message', Hemelios_Install_DemoData.htmlTag.wrapper).addClass('error');
                            $('.install-message', Hemelios_Install_DemoData.htmlTag.wrapper).text(data.message);
                            window.onbeforeunload = null;
                            Hemelios_Install_DemoData.vars.is_install = false;
                            break;

                        case 'setting':
                            Hemelios_Install_DemoData.animate_percent('#hemelios_reset_option > span');
                            Hemelios_Install_DemoData.install(data.code, method, '');
                            break;

                        case 'core':
                            $('#hemelios_reset_option > span').css({width : '100%'});
                            $('#hemelios_reset_option').addClass('nostripes');
                            var arr_core_progress = data.message.split('|');
                            if (arr_core_progress.length >= 2) {
                                percent = arr_core_progress[0]/arr_core_progress[1] * 100;
                            }

                            Hemelios_Install_DemoData.animate_percent('#hemelios_install_demo > span', percent);
                            Hemelios_Install_DemoData.install(data.code, method, data.message);
                            break;

                        case 'slider':
                            Hemelios_Install_DemoData.vars.try_install_slider = 0;
                            jQuery('#hemelios_install_demo > span').css({width : '100%'});
                            jQuery('#hemelios_install_demo').addClass('nostripes');

                            Hemelios_Install_DemoData.animate_loading('#hemelios_import_slider > span');
                            Hemelios_Install_DemoData.install(data.code, method, data.message);
                            break;

                        case 'done':
                            jQuery('#hemelios_import_slider > span').css({width : '100%'});
                            jQuery('#hemelios_import_slider').addClass('nostripes');

                            $('.install-message', Hemelios_Install_DemoData.htmlTag.wrapper).addClass('updated');
                            Hemelios_Install_DemoData.vars.is_install = false;
                            window.onbeforeunload = null;
                            break;
                        default:
                            if (type == 'slider') {
                                Hemelios_Install_DemoData.vars.try_install_slider +=1;
                                if (Hemelios_Install_DemoData.vars.try_install_slider < 10) {
                                    Hemelios_Install_DemoData.install(type, method, other_data);
                                }
                                else {
                                    Hemelios_Install_DemoData.install('fix-data', method, '');
                                }
                            }
                            else {
                                Hemelios_Install_DemoData.install(type, method, other_data);
                            }
                            break;
                    }
                }
            });
        },

        animate_percent: function(processbar, percent) {
            if (percent > 100) return;
            $(processbar).css({width:  percent + '%'});
        },
        animate_loading: function(processbar) {
            if ($(processbar).attr('style') == 'width: 100%;') {
                return;
            }
            var width = parseInt(jQuery(processbar).width(),10);
            var parentWidth = parseInt($(processbar).parent().width(),10);
            var percent = (width*1.0  / parentWidth) * 100  + 1;

            if (percent > 100) return;
            if (percent < 98)
            {
                $(processbar).css({width:  percent + '%'});
                setTimeout(function() {
                    Hemelios_Install_DemoData.animate_loading(processbar);
                },500);
            }
        }
    }
    $(document).ready(function(){
        Hemelios_Install_DemoData.initialize();
    });
})(jQuery);