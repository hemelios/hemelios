<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/1/2015
 * Time: 10:27 AM
 */
/*================================================
HEAD META
================================================== */
if ( !function_exists( 'hemelios_head_meta' ) ) {
	function hemelios_head_meta() {
		hemelios_get_template( 'head-meta' );
	}

	add_action( 'wp_head', 'hemelios_head_meta', 0 );
}

/*================================================
SOCIAL META
================================================== */
if ( !function_exists( 'hemelios_add_opengraph_doctype' ) ) {
	function hemelios_add_opengraph_doctype( $output ) {
		$hemelios_options = hemelios_option();
		$enable_social_meta = false;
		if ( isset( $hemelios_options['enable_social_meta'] ) ) {
			$enable_social_meta = $hemelios_options['enable_social_meta'];
		}
		if ( !$enable_social_meta ) {
			return $output;
		}

		return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';

	}

	add_filter( 'language_attributes', 'hemelios_add_opengraph_doctype' );
}

if ( !function_exists( 'hemelios_social_meta' ) ) {
	function hemelios_social_meta() {
		hemelios_get_template( 'social-meta' );
	}

	add_action( 'wp_head', 'hemelios_social_meta', 5 );
}

