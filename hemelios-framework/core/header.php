<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/1/2015
 * Time: 5:50 PM
 */
/*================================================
SITE LOADING
================================================== */
if ( !function_exists( 'hemelios_site_loading' ) ) {
	function hemelios_site_loading() {
		hemelios_get_template( 'site-loading' );
	}

	add_action( 'hemelios_before_page_wrapper', 'hemelios_site_loading', 5 );
}

/*================================================
MOBILE FLY BODY OVERLAY
================================================== */
if ( !function_exists( 'hemelios_main_menu_overlay' ) ) {
	function hemelios_main_menu_overlay() {
		hemelios_get_template( 'main-menu-overlay' );
	}

	add_action( 'hemelios_main_menu_after', 'hemelios_main_menu_overlay', 10 );
}


/*================================================
BODY CLASS
================================================== */
if ( !function_exists( 'hemelios_body_class_name' ) ) {
	function hemelios_body_class_name( $classes ) {
		global $hemelios_options;
		$prefix = 'hemelios_';

		$classes[] = 'footer-static';
		if ( $hemelios_options['home_preloader'] != 'none' && !empty( $hemelios_options['home_preloader'] ) ) {
			$classes[] = 'site-loading';
		}

		$layout_style = hemelios_get_post_meta_box_option( $prefix . 'layout_style' );
		if ( ( $layout_style == '' ) || ( $layout_style == "-1" ) ) {
			$layout_style = $hemelios_options['layout_style'];
		}
		if ( $layout_style == 'boxed' ) {
			$classes[] = 'boxed';
		}

		$page_class_extra = hemelios_get_post_meta_box_option( $prefix . 'page_class_extra', 'type=text', get_the_ID() );

		if ( !empty( $page_class_extra ) ) {
			$classes[] = $page_class_extra;
		}
		//Header position class

		$header_overlay = hemelios_get_post_meta_box_option( $prefix . 'header_positon' );
		if ( ( $header_overlay === '' ) || ( $header_overlay == '-1' ) ) {
			$header_overlay = $hemelios_options['header_positon'];
		}
		if ( $header_overlay == '1' ) {
			$classes[] = 'header-overlay';
		}
		// 404 page style
		if ( is_404() ) {
			$classes[] = 'header-overlay';
		}

		$header_layout = hemelios_get_post_meta_box_option( $prefix . 'header_layout' );
		if ( ( $header_layout === '' ) || ( $header_layout == '-1' ) ) {
			$header_layout = $hemelios_options['header_layout'];
		}
		$classes[] = $header_layout;


		return $classes;
	}

	add_filter( 'body_class', 'hemelios_body_class_name' );
}

if( !function_exists('hemelios_custom_style_header') ){
	function hemelios_custom_style_header(){
		$hemelios_options          = hemelios_option();
		$prefix                  = 'hemelios_';
		//SET header color
		$custom_style              = array();
		$header_background_color   = hemelios_get_post_meta_box_option( $prefix . 'header_bg_color' );
		$header_background_opacity = hemelios_get_post_meta_box_option( $prefix . 'header_bg_opacity' );


		//TOP BAR BORDER
		$top_bar_border_color = hemelios_get_post_meta_box_option( $prefix . 'topbar_border_color' );
		if ( $top_bar_border_color == '' ) {
			$top_bar_border_color = $hemelios_options['top_bar_border_color'];
		}

		// SHOW TOP BAR BORDER
		$show_topbar_border = hemelios_get_post_meta_box_option( $prefix . 'show_top_bar_border' );
		if ( $show_topbar_border == '-1' || $show_topbar_border == '' ) {
			$show_topbar_border = $hemelios_options['show_top_bar'];
		}


		// HEADER STICKY BORDER
		$header_sticky_border = hemelios_get_post_meta_box_option( $prefix . 'header_sticky_border' );

		if ( $header_sticky_border == '' || $header_sticky_border == '-1' ) {
			$header_sticky_border = $hemelios_options['header_sticky_border'];
		}

		// Header Sticky Background Color
		$header_sticky_bg = hemelios_get_post_meta_box_option( $prefix . 'header_sticky_bg_color' );



		$custom_opacity = hemelios_get_post_meta_box_option( $prefix . 'header_custom_overlay_opacity' );
		$header_bg      = '';
		if ( !empty( $header_background_color ) ) {
			if ( $custom_opacity == '1' ) {
				$alpha = $header_background_opacity / 100;
				if ( $alpha == 0 ) {
					$header_bg = hemelios_hex2rgba( $header_background_color, '0' );
				} else {
					$header_bg = hemelios_hex2rgba( $header_background_color, $alpha );
				}
			} else {
				$header_bg = $header_background_color;
			}
		}

		$menu_background_color = hemelios_get_post_meta_box_option( $prefix . 'menu_bg_color' );
		$menu_text_color       = hemelios_get_post_meta_box_option( $prefix . 'menu_text_color' );
		$menu_text_hover_color = hemelios_get_post_meta_box_option( $prefix . 'menu_text_hover_color' );
		$header_margin_top     = hemelios_get_post_meta_box_option( $prefix . 'header_margin_top' );

		$topbar_bg_color = hemelios_get_post_meta_box_option( $prefix . 'topbar_bg_color' );
		$topbar_color    = hemelios_get_post_meta_box_option( $prefix . 'topbar_color' );

		// HEADER SHOW BORDER
		$header_border_color   = hemelios_get_post_meta_box_option( $prefix . 'header_border_color' );
		$header_border_opacity = hemelios_get_post_meta_box_option( $prefix . 'header_border_opacity' );

		$header_show_border = hemelios_get_post_meta_box_option( $prefix . 'header_show_border' );

		if ( $header_show_border == '-1' || $header_show_border == '' || $header_show_border == '0' ) {
			if ( $header_show_border != '0' ) {
				if ( isset( $hemelios_options['header_border'] ) ) {
					$header_show_border = $hemelios_options['header_border'];
				}
			}
			// GET HEADER BORDER COLOR
			$alpha = 0;
			$color = '#eee';

			if ( isset( $hemelios_options['header_border_color']['alpha'] ) ) {
				$alpha = $hemelios_options['header_border_color']['alpha'];
			}
			if ( isset( $hemelios_options['header_border_color']['color'] ) ) {
				$color = $hemelios_options['header_border_color']['color'];
			}
			$header_border_color = hemelios_hex2rgba( $color, $alpha );
		} else {
			// GET HEADER BORDER COLOR
			if ( !empty( $header_border_color ) ) {
				$alpha = $header_border_opacity / 100;
				if ( $alpha == 0 ) {
					$header_border_color = hemelios_hex2rgba( $header_border_color, '0' );
				} else {
					$header_border_color = hemelios_hex2rgba( $header_border_color, $alpha );
				}
			}
		}


		// CUSTOM CART QUANTITY ICON BACKGROUND COLOR
		$custom_cart_quantity_icon_bg = hemelios_get_post_meta_box_option( $prefix . 'custom_cart_quantity_icon_bg' );


		if ( empty( $custom_cart_quantity_icon_bg ) ) {
			if ( isset( $hemelios_options['custom_cart_quantity_icon_bg'] ) && !empty( $hemelios_options['custom_cart_quantity_icon_bg'] ) ) {
				$custom_cart_quantity_icon_bg = $hemelios_options['custom_cart_quantity_icon_bg'];
			}
		}

		// CUSTOM AND LOGO TOP/BOTTOM PADDING
		$custom_logo_padding = hemelios_get_post_meta_box_option( $prefix . 'custom_logo_padding' );

		// Default logo padding
		$hemelios_header_layout = hemelios_get_post_meta_box_option( 'hemelios_header_layout' );
		if ( ( $hemelios_header_layout === '' ) || ( $hemelios_header_layout == '-1' ) ) {
			$hemelios_header_layout = $hemelios_options['header_layout'];
		}

		$logo_max_height = 27;

		if ( isset( $hemelios_options['logo_max_height'] ) && isset( $hemelios_options['logo_max_height']['height'] ) &&
				$hemelios_options['logo_max_height']['height'] != 'px'
		) {
			$logo_max_height = intval( str_replace( 'px', '', $hemelios_options['logo_max_height']['height'] ) );
		}

		if ( !empty( $custom_logo_padding ) ) {
			// default_logo_max_height is 27
			$custom_style[] = 'header.main-header .header-logo > a > img { padding-top: ' . $custom_logo_padding . 'px !important; padding-bottom: ' . $custom_logo_padding . 'px !important; max-height: ' . ( $logo_max_height + $custom_logo_padding * 2 ) . 'px !important;}';
			$custom_style[] = 'header.header-2 .header-menu .menu-wrapper .header-customize  { top: -' . ( ( $logo_max_height + $custom_logo_padding * 2 + 50 ) / 2 ) . 'px; }';

		}

		// SHOW TOP BAR BORDER
		if ( $show_topbar_border == '1' ) {
			$custom_style[] = '.top-bar{border-bottom: 1px solid ' . $top_bar_border_color . ' !important;}';
			$custom_style[] = '.top-bar{border-top: 1px solid ' . $top_bar_border_color . ' !important;}';
		} elseif ( $show_topbar_border == '0' ) {
			$custom_style[] = '.top-bar{border: none !important;}';
		}

		// SHOW HEADER STICKY BORDER
		if ( $header_sticky_border == '1' ) {
			$custom_style[] = '.sticky-wrapper.is-sticky header.main-header{border-bottom: 1px solid ' . $header_border_color . ' !important; -webkit-box-shadow: 0px 1px 4px 0px rgba(49, 50, 50, 0.46); -moz-box-shadow:    0px 1px 4px 0px rgba(49, 50, 50, 0.46); box-shadow: 0px 1px 4px 0px rgba(49, 50, 50, 0.46);}';

			$custom_style[] = '@media screen and (min-width: 992px){ header.header-2 .sticky-wrapper.is-sticky .header-menu{border-bottom: 1px solid ' . $header_border_color . ' !important; -webkit-box-shadow: 0px 1px 4px 0px rgba(49, 50, 50, 0.46);-moz-box-shadow: 0px 1px 4px 0px rgba(49, 50, 50, 0.46); box-shadow: 0px 1px 4px 0px rgba(49, 50, 50, 0.46);}}';

			$custom_style[] = '@media screen and (min-width: 992px){.sticky-wrapper.is-sticky .header-3-menu-wrapper {border-bottom: 1px solid ' . $header_border_color . ' !important; -webkit-box-shadow: 0px 1px 4px 0px rgba(49, 50, 50, 0.46); -moz-box-shadow: 0px 1px 4px 0px rgba(49, 50, 50, 0.46); box-shadow: 0px 1px 4px 0px rgba(49, 50, 50, 0.46);}}';

			$custom_style[] = '@media screen and (min-width: 992px){.sticky-wrapper.is-sticky ul#main-menu li > ul.x-sub-menu { border-top: 1px solid transparent; background-clip: padding-box;}}';
		}

		// Header Sticky Background Color
		if ($header_sticky_bg != '') {
			$custom_style[] = '@media screen and (min-width: 992px){ .sticky-wrapper.is-sticky header.main-header { background-color: ' . $header_sticky_bg . ' !important;} .sticky-wrapper.is-sticky header.main-header .menu-wrapper  { background-color: ' . $header_sticky_bg . ' !important;}}';

			$custom_style[] = '@media screen and (min-width: 992px){ header.header-2 .sticky-wrapper.is-sticky .header-2-menu-wrapper { background-color: ' . $header_sticky_bg . ' !important; }  header.header-2 .sticky-wrapper.is-sticky .header-2-menu-wrapper .menu-wrapper { background-color: ' . $header_sticky_bg . ' !important; }}';

			$custom_style[] = '@media screen and (min-width: 992px){ header.header-3 .sticky-wrapper.is-sticky .header-3-menu-wrapper { background-color: ' . $header_sticky_bg . ' !important; } header.header-3 .sticky-wrapper.is-sticky .header-3-menu-wrapper .menu-wrapper {  background-color: ' . $header_sticky_bg . ' !important; }}';
		}


		if ( $topbar_bg_color != '' ) {
			$custom_style[] = '.top-bar{background-color: ' . $topbar_bg_color . ' !important;}';
		}
		if ( $topbar_color != '' ) {
			$custom_style[] = '.top-bar .sidebar{color: ' . $topbar_color . ' !important;}';
		}
		if ( $header_show_border == '1' ) {
			if ( $header_border_color != '' ) {
				$custom_style[] = 'header.main-header {border-bottom: 1px solid ' . $header_border_color . ' !important; border-top: 1px solid ' . $header_border_color . ' !important; } ul#main-menu li > ul.x-sub-menu { margin-top: 0px; border-top: 1px solid transparent; background-clip: padding-box;} ';
			}
		}
		if ( $header_background_color != '' ) {
			$custom_style[] = 'body.header-overlay header.main-header{background-color: ' . $header_bg . ' !important;} ';
			$custom_style[] = 'header.header-2 .header-menu{background-color: ' . $header_bg . ' !important;} ';
			$custom_style[] = 'header.main-header{background-color: ' . $header_bg . ' !important} ';
			$custom_style[] = '@media screen and (max-width: 991px){ body.header-overlay header.main-header.header-mobile-1{background-color: #111 !important;} } ';
			$custom_style[] = '@media screen and (max-width: 991px){ body.header-overlay header.main-header.header-mobile-2{background-color: #fff !important;} }';
			$custom_style[] = '@media screen and (max-width: 991px){ body.header-overlay header.main-header.header-mobile-3{background-color: #111 !important;} }';
		}
		if ( $header_margin_top != '' ) {
			$custom_style[] = 'header.main-header {margin-top: ' . $header_margin_top . 'px !important;}';
		}

		if ( $menu_background_color != '' ) {
			$custom_style[] = 'header.main-header .menu-wrapper { background-color: ' . $menu_background_color . ' !important;}';
			$custom_style[] = 'header.header-2 .header-menu{background-color: ' . $menu_background_color . ' !important;}';
			$custom_style[] = 'header.header-3 .header-3-menu-wrapper{background-color: ' . $menu_background_color . ' !important;}';
		}
		if ( $menu_text_color != '' ) {
			$custom_style[] = 'header.main-header .menu-wrapper .x-nav-menu > li.x-menu-item > a.x-menu-a-text > span {color: ' . $menu_text_color . ';}';
			$custom_style[] = 'header.main-header .menu-wrapper .x-nav-menu > li.x-menu-item > a.x-menu-a-text > b.x-caret:before {color: ' . $menu_text_color . ';}';
//		$custom_style[] = 'header.header-3 .menu-wrapper .x-nav-menu>li.x-menu-item>a{color: ' . $menu_text_color . ' !important;}';
//		$custom_style[] = 'header.header-2 .menu-wrapper .x-nav-menu>li.x-menu-item>a{color: ' . $menu_text_color . ' !important;}';
			$custom_style[] = '#primary-menu .header-customize .shopping-cart-wrapper .widget_shopping_cart_icon_wrapper p.cart-subtotal { color: ' . $menu_text_color . ' !important;}';
			$custom_style[] = '#primary-menu .header-customize .shopping-cart-wrapper .widget_shopping_cart_icon_wrapper .widget_shopping_cart_icon > i.icon { color: ' . $menu_text_color . ' !important;}';
		}


		if ( !empty( $menu_text_hover_color ) ) {
			$custom_style[] = 'header.main-header .menu-wrapper .x-nav-menu > li.current-menu-ancestor > a.x-menu-a-text > span, header.main-header .menu-wrapper .x-nav-menu > li.current-menu-parent > a.x-menu-a-text > span, header.main-header .menu-wrapper .x-nav-menu > li.current-menu-item > a.x-menu-a-text > span, header.main-header .menu-wrapper .x-nav-menu > li.menu-current > a.x-menu-a-text > span, header.main-header .menu-wrapper .x-nav-menu > li:hover > a.x-menu-a-text > span { color: ' . $menu_text_hover_color . ' !important; }';
			$custom_style[] = 'header.main-header .menu-wrapper .x-nav-menu > li.current-menu-ancestor > a.x-menu-a-text > span:before, header.main-header .menu-wrapper .x-nav-menu > li.current-menu-parent > a.x-menu-a-text > span:before, header.main-header .menu-wrapper .x-nav-menu > li.current-menu-item > a.x-menu-a-text > span:before, header.main-header .menu-wrapper .x-nav-menu > li.menu-current > a.x-menu-a-text > span:before, header.main-header .menu-wrapper .x-nav-menu > li:hover > a.x-menu-a-text > span:before { border-top-color: ' . $menu_text_hover_color . ' !important; }';
			$custom_style[] = 'header.main-header .menu-wrapper .x-nav-menu > li.current-menu-ancestor > a.x-menu-a-text > b.x-caret:before, header.main-header .menu-wrapper .x-nav-menu > li.current-menu-parent > a.x-menu-a-text > b.x-caret:before, header.main-header .menu-wrapper .x-nav-menu > li.current-menu-item > a.x-menu-a-text > b.x-caret:before, header.main-header .menu-wrapper .x-nav-menu > li.menu-current > a.x-menu-a-text > b.x-caret:before, header.main-header .menu-wrapper .x-nav-menu > li:hover > a.x-menu-a-text > b.x-caret:before { color: ' . $menu_text_hover_color . ' !important; }';
			$custom_style[] = 'header.main-header .menu-wrapper .x-nav-menu li.current-menu-ancestor > a.x-menu-a-text > span, header.main-header .menu-wrapper .x-nav-menu li.current-menu-parent > a.x-menu-a-text > span, header.main-header .menu-wrapper .x-nav-menu li.current-menu-item > a.x-menu-a-text > span, header.main-header .menu-wrapper .x-nav-menu li.menu-current > a.x-menu-a-text > span { color: ' . $menu_text_hover_color . ' !important; }';
			$custom_style[] = 'header.main-header .menu-wrapper .x-nav-menu li.current-menu-ancestor > a.x-menu-a-text > b.x-caret:before, header.main-header .menu-wrapper .x-nav-menu li.current-menu-parent > a.x-menu-a-text > b.x-caret:before, header.main-header .menu-wrapper .x-nav-menu li.current-menu-item > a.x-menu-a-text > b.x-caret:before, header.main-header .menu-wrapper .x-nav-menu li.menu-current > a.x-menu-a-text > b.x-caret:before { color: ' . $menu_text_hover_color . ' !important; }';
		}

		if ( !empty( $custom_cart_quantity_icon_bg ) ) {
			$custom_style[] = '#primary-menu .header-customize .shopping-cart-wrapper .widget_shopping_cart_icon > span { background-color: ' . $custom_cart_quantity_icon_bg . ' !important;} ';
		}

		// Print $custom_404_style;
		$custom_404_style = hemelios_404_custom_style();
		if ( isset( $custom_404_style ) && !empty( $custom_404_style ) ) {
			$custom_style = array_merge( $custom_style, $custom_404_style );
		}

		if ( $custom_style ) {
			$custom_css = join( ' ', $custom_style );
			echo hemelios_style_schemes_output( $custom_css );
		}
	}
	add_action('wp_head', 'hemelios_custom_style_header', 999999);

}

/*================================================
PAGE TITLE
================================================== */
if ( !function_exists( 'hemelios_page_title' ) ) {
	function hemelios_page_title() {
		hemelios_get_template( 'page-title' );
	}

	add_action( 'hemelios_before_page', 'hemelios_page_title', 5 );
}
/*================================================
ARCHIVE HEADING
================================================== */
if ( !function_exists( 'hemelios_archive_title' ) ) {
	function hemelios_archive_title() {
		hemelios_get_template( 'archive-title' );
	}

	add_action( 'hemelios_before_archive', 'hemelios_archive_title', 5 );
}

if ( !function_exists( 'hemelios_archive_product_title' ) ) {
	function hemelios_archive_product_title() {
		hemelios_get_template( 'archive-product-title' );
	}

	add_action( 'hemelios_before_archive_product', 'hemelios_archive_product_title', 5 );
}
/*================================================
SEARCH HEADER
================================================== */
//if (!function_exists('hemelios_page_top_drawer')) {
//	function hemelios_search_drawer() {
//		hemelios_get_template('header/search','popup');
//	}
//	add_action('hemelios_before_page_wrapper_content','hemelios_search_drawer',5);
//}
/*================================================
ABOVE HEADER
================================================== */
//if ( !function_exists( 'hemelios_page_top_drawer' ) ) {
//	function hemelios_page_top_drawer() {
//		hemelios_get_template( 'top-drawer-template' );
//	}
//
//	add_action( 'hemelios_before_page_wrapper_content', 'hemelios_page_top_drawer', 10 );
//}

/*================================================
TOP BAR
================================================== */
if ( !function_exists( 'hemelios_page_top_bar' ) ) {
	function hemelios_page_top_bar() {
		hemelios_get_template( 'top-bar-template' );
	}

	add_action( 'hemelios_before_page_wrapper_content', 'hemelios_page_top_bar', 10 );
}

/*================================================
HEADER
================================================== */
if ( !function_exists( 'hemelios_page_header' ) ) {
	function hemelios_page_header() {
		hemelios_get_template( 'header-template' );
	}

	add_action( 'hemelios_before_page_wrapper_content', 'hemelios_page_header', 15 );
}